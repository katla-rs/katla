# Katla
A katla configuration consists of the following elements

- [flows](#flow)
- [feeds](#feed)
- [entries](#entry)
- [entity providers](#entity-provider)
- [appenders](#appender)

## Format
Both json and ron format are support.

## Flow
there can be one or more flows
a flow has entries as its data and one ore more outputs which can be configured to send output at different moments in time

an output consists of a feed and an appender together with a duration for controlling when the output is generated.

## Appender
An appender gets its content from one or more [feeds](#feed) and writes this content to a file, kafka queue, ...

## Feed
a feed is a thread that will produce the content in a (un)strucutured way as ie csv, sql, xml, ... and afterwards feeded to the configured appender in the flows output.

## Entry
an entry conists of one or more entities.

## Entity Provider
and an entityProviders define entities which can be a sequence, uuid, random data, script evaluation, map of entities, timestamp, ...
