// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::Appender;
use extcsv::{Writer, WriterBuilder};
use std::{collections::HashMap, fs::File};

#[derive(Debug)]
pub struct Csv {
    pub appender: Appender,
    pub csv_writers: HashMap<String, Writer<File>>,
}

impl AppenderType for Csv {
    fn start(&mut self) {
        if let Appender::Csv { name, file_name } = self.appender.clone() {
            let csv_file = File::create(file_name).unwrap();
            let csv_writer = WriterBuilder::new().delimiter(b';').from_writer(csv_file);
            //csv_writer.write_record(csv_header.iter()).unwrap();
            self.csv_writers.insert(name.to_string(), csv_writer);
        }
    }

    fn appenders(&mut self, _appenders: std::collections::HashMap<String, super::AppenderThread>) {}

    fn append(&mut self, content: AppenderContent, flush: bool) {
        if let Appender::Csv { name, file_name: _ } = self.appender.clone() {
            let csv_writer = self.csv_writers.get_mut(&name).unwrap();
            let mut csv_columns = Vec::new();
            match content {
                AppenderContent::Binary { data } => csv_columns.push(data),
                AppenderContent::String { data } => csv_columns.push(data.as_bytes().to_vec()),
                AppenderContent::Array { values, .. } => csv_columns.append(
                    &mut values
                        .iter()
                        .map(|value| value.as_bytes().to_vec())
                        .collect(),
                ),
            }
            csv_writer
                .write_record(csv_columns.iter())
                .expect("Unable to write data");
            if flush {
                csv_writer.flush().unwrap();
            }
        }
    }

    fn stop(&mut self) {
        if let Appender::Csv { name, file_name: _ } = self.appender.clone() {
            // A CSV writer maintains an internal buffer, so it's important
            // to flush the buffer when you're done.
            let csv_writer = self.csv_writers.get_mut(&name).unwrap();
            csv_writer.flush().unwrap();
        }
    }
}

#[cfg(test)]
mod tests {
    //use super::*;
}
