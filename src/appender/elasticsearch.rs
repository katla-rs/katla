// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::{Appender, ElasticsearchConnection, ElasticsearchCredentials};
use std::collections::HashMap;

use elasticsearch::auth::Credentials;
use elasticsearch::http::request::JsonBody;
use elasticsearch::http::transport::{
    CloudConnectionPool, SingleNodeConnectionPool, TransportBuilder,
};
use elasticsearch::http::Url;
use elasticsearch::BulkParts;
use serde_json::{json, Value};
use threadpool::ThreadPool;
use tokio::runtime::{Handle, Runtime};

//#[derive(Debug)]
pub struct Elasticsearch {
    pub appender: Appender,
    pub elasticsearch_clients: HashMap<String, elasticsearch::Elasticsearch>,
    pub data: HashMap<String, Vec<JsonBody<Value>>>,
    pub tokio_runtime: Runtime,
    pub thread_pool: ThreadPool,
}

impl AppenderType for Elasticsearch {
    fn start(&mut self) {
        if let Appender::Elasticsearch {
            name,
            connection,
            credentials,
            ..
        } = self.appender.clone()
        {
            let transport_builder = match connection {
                ElasticsearchConnection::SingleNode { url } => {
                    TransportBuilder::new(SingleNodeConnectionPool::new(Url::parse(&url).unwrap()))
                }
                ElasticsearchConnection::Cloud { id } => {
                    TransportBuilder::new(CloudConnectionPool::new(&id).unwrap())
                }
            };
            let transport_builder = match credentials {
                Some(credentials) => {
                    let credentials = match credentials {
                        ElasticsearchCredentials::Basic { username, password } => {
                            Credentials::Basic(username.into(), password.into())
                        }
                        ElasticsearchCredentials::Bearer { access_token } => {
                            Credentials::Bearer(access_token.into())
                        }
                        ElasticsearchCredentials::ApiKey { id, api_key } => {
                            Credentials::ApiKey(id.into(), api_key.into())
                        }
                    };
                    transport_builder.auth(credentials)
                }
                None => transport_builder,
            };
            let transport = transport_builder
                .cert_validation(elasticsearch::cert::CertificateValidation::None)
                .disable_proxy()
                .build()
                .unwrap();
            let elasticsearch_client = elasticsearch::Elasticsearch::new(transport);
            self.elasticsearch_clients
                .insert(name.to_string(), elasticsearch_client);
        }
    }

    fn appenders(&mut self, _appenders: std::collections::HashMap<String, super::AppenderThread>) {}

    fn append(&mut self, content: AppenderContent, flush: bool) {
        if let Appender::Elasticsearch { name, index, .. } = self.appender.clone() {
            let elasticsearch_client = self.elasticsearch_clients.get_mut(&name).unwrap();
            let append_data = self
                .data
                .entry(name.clone())
                .or_insert(Vec::with_capacity(2));
            match content {
                AppenderContent::Binary { data: _ } => {},
                AppenderContent::String { data } => {
                    append_data.push(json!({"index": {"_index": format!("{}", index)}}).into());
                    append_data.push(json!(serde_json::from_str::<Value>(&data).unwrap()).into());
                }
                AppenderContent::Array { values, .. } => {
                    // TODO create json from values so we also need the keys here
                    let mut body: Vec<JsonBody<_>> = Vec::with_capacity(values.len() * 2);
                    let mut index_id = 0;
                    for value in values {
                        index_id += 1;
                        body.push(json!({"index": {"_id": format!("{}", index_id)}}).into());
                        body.push(json!(serde_json::from_str::<Value>(&value).unwrap()).into());
                    }

                    let response = self
                        .tokio_runtime
                        .block_on(
                            elasticsearch_client
                                .bulk(BulkParts::Index(&index))
                                .body(body)
                                .send(), /*.await */
                        )
                        .unwrap();

                    let response_body = self
                        .tokio_runtime
                        .block_on(response.json::<Value>() /*.await */)
                        .unwrap();
                    let _successful = response_body["errors"].as_bool().unwrap() == false;
                }
            }
            if flush {
                if let Some(data) = self.data.remove(&name) {
                    Self::flush(
                        elasticsearch_client,
                        data,
                        &self.tokio_runtime.handle(),
                        &self.thread_pool,
                    );
                }
            }
        }
    }

    fn stop(&mut self) {
        if let Appender::Elasticsearch { name, .. } = self.appender.clone() {
            let elasticsearch_client = self.elasticsearch_clients.get_mut(&name).unwrap();
            if let Some(data) = self.data.remove(&name) {
                Self::flush(
                    elasticsearch_client,
                    data,
                    &self.tokio_runtime.handle(),
                    &self.thread_pool,
                );
            }
        }
    }
}

impl Elasticsearch {
    fn flush(
        elasticsearch_client: &mut elasticsearch::Elasticsearch,
        data: Vec<JsonBody<Value>>,
        handle: &Handle,
        thread_pool: &ThreadPool,
    ) {
        let elasticsearch_client = elasticsearch_client.clone();
        let handle = handle.clone();
        thread_pool.execute(move || {
            handle.block_on(async {
                let response = elasticsearch_client
                    .bulk(BulkParts::None)
                    .body(data)
                    .send()
                    .await
                    .unwrap();

                if response.status_code().is_client_error() {
                    error!("error = {:#?}", response.text().await.unwrap());
                } else {
                    let response_body = response.json::<Value>().await.unwrap();

                    let _successful = response_body["errors"].as_bool().unwrap() == false;
                }
            });
        });
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[ignore]
    #[test]
    fn test_appender_elasticsearch_single_node_bulk_insert() {
        let appender: Appender = ron::from_str(
            &r#"#![enable(implicit_some)]
                    ( 
                        type: "elasticsearch",
                        name: "elasticsearch-appender",
                        connection: (
                            type: "singleNode",
                            url: "https://localhost:9200/",
                        ),
                        credentials: (
                            type: "basic",
                            username: "elastic",
                            password: "********",
                        ),
                        index: "test",
                    )"#,
        )
        .unwrap();

        let mut elasticsearch = Box::new(Elasticsearch {
            appender: appender,
            elasticsearch_clients: HashMap::new(),
            data: HashMap::new(),
            tokio_runtime: Runtime::new().unwrap(),
            thread_pool: ThreadPool::new(1),
        });

        elasticsearch.start();
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 1,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 1 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 2,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 2 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 3,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 3 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 4,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 4 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 5,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 5 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 6,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 6 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 7,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 7 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 8,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 8 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.append(
            AppenderContent::String {
                data: r#"{
            "id": 9,
            "user": "kimchy",
            "post_date": "2023-09-08T17:50:54Z",
            "message": "Message 9 -- Trying out Elasticsearch, so far so good?"
    }
"#
                .to_string(),
            },
            false,
        );
        elasticsearch.stop();
    }
}
