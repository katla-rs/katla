// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::Appender;
use std::collections::HashMap;

extern crate kafka;
use kafka::producer::{Producer, Record, RequiredAcks};
use std::time::Duration;

//#[derive(Debug)]
pub struct Kafka {
    pub appender: Appender,
    pub kafka_producers: HashMap<String, Producer>,
}

impl AppenderType for Kafka {
    fn start(&mut self) {
        if let Appender::Kafka {
            name, host, port, ..
        } = self.appender.clone()
        {
            let brokers = vec![format!("{}:{}", host, port).to_owned()];
            let kafka_producer = Producer::from_hosts(brokers)
                // ~ give the brokers one second time to ack the message
                .with_ack_timeout(Duration::from_secs(1))
                // ~ require only one broker to ack the message
                .with_required_acks(RequiredAcks::One)
                // ~ build the producer with the above settings
                .create()
                .unwrap();
            self.kafka_producers
                .insert(name.to_string(), kafka_producer);
        }
    }

    fn appenders(&mut self, _appenders: std::collections::HashMap<String, super::AppenderThread>) {}

    fn append(&mut self, content: AppenderContent, _flush: bool) {
        if let Appender::Kafka { name, topic, .. } = self.appender.clone() {
            let kafka_producer = self.kafka_producers.get_mut(&name).unwrap();
            //producer.send(&Record::from_value(topic, data));
            //producer.send(&Record { topic: topic, partition: -1, key: (), value: data, });
            match content {
                AppenderContent::Binary { data: _ } => {},
                AppenderContent::String { data } => {
                    let msg = &Record::from_value(&topic, data);
                    kafka_producer.send(msg).unwrap();
                }
                AppenderContent::Array { values, .. } => {
                    for value in values {
                        let msg = &Record::from_value(&topic, value.to_string());
                        kafka_producer.send(msg).unwrap();
                    }
                }
            }
        }
    }

    fn stop(&mut self) {
        if let Appender::Kafka { name, .. } = self.appender.clone() {
            let _kafka_producer = self.kafka_producers.get_mut(&name).unwrap();
            // kafka_producer.stop();
        }
    }
}

#[cfg(test)]
mod tests {
    //use super::*;
}
