// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

pub mod appender;
pub mod csv;
pub mod directory;
#[cfg(feature = "elastic-elasticsearch")]
pub mod elasticsearch;
pub mod file;
#[cfg(feature = "kafka")]
pub mod kafka;
#[cfg(feature = "ibm-mq")]
pub mod mq;
pub mod void;

use crate::config::appender::Appender as ConfigAppender;

use self::appender::Appender;
use self::csv::Csv;
use self::directory::Directory;
#[cfg(feature = "elastic-elasticsearch")]
use self::elasticsearch::Elasticsearch;
use self::file::File;
#[cfg(feature = "kafka")]
use self::kafka::Kafka;
#[cfg(feature = "ibm-mq")]
use self::mq::Mq;
use self::void::Void;

use super::Identifier;
use std::collections::HashMap;
use std::error::Error;
use std::{
    sync::mpsc::{channel, Receiver, Sender},
    //	thread,
    thread::JoinHandle,
};
use threadpool::ThreadPool;
#[cfg(feature = "elastic-elasticsearch")]
use tokio::runtime::Runtime;

trait AppenderType {
    fn start(&mut self);
    fn appenders(&mut self, appenders: HashMap<String, AppenderThread>);
    fn append(&mut self, content: AppenderContent, flush: bool);
    fn stop(&mut self);
}

fn into(appender: ConfigAppender) -> Box<dyn AppenderType> {
    match appender {
        ConfigAppender::Appender { .. } => Box::new(Appender {
            appender: appender,
            appenders: HashMap::new(),
        }),
        ConfigAppender::Csv { .. } => Box::new(Csv {
            appender: appender,
            csv_writers: HashMap::new(),
        }),
        ConfigAppender::Directory { .. } => Box::new(Directory {
            appender: appender,
            directory_counters: HashMap::new(),
        }),
        #[cfg(feature = "elastic-elasticsearch")]
        ConfigAppender::Elasticsearch { threads, .. } => Box::new(Elasticsearch {
            appender: appender,
            elasticsearch_clients: HashMap::new(),
            data: HashMap::new(),
            tokio_runtime: Runtime::new().unwrap(),
            thread_pool: ThreadPool::new(threads.unwrap_or_else(|| 1)),
        }),
        ConfigAppender::File { .. } => Box::new(File {
            appender: appender,
            file_writers: HashMap::new(),
        }),
        #[cfg(feature = "kafka")]
        ConfigAppender::Kafka { .. } => Box::new(Kafka {
            appender: appender,
            kafka_producers: HashMap::new(),
        }),
        #[cfg(feature = "ibm-mq")]
        ConfigAppender::Mq { .. } => Box::new(Mq {
            appender: appender,
            jni_env: None,
            jvm: None,
        }),
        ConfigAppender::Void { .. } => Box::new(Void { appender }),
    }
}

impl ConfigAppender {
    pub fn start(&mut self) -> std::result::Result<AppenderThread, Box<dyn Error>> {
        let identifier = self.get_identifier();
        let (appender_sender, appender_receiver): (
            Sender<AppenderAction>,
            Receiver<AppenderAction>,
        ) = channel();
        appender_sender.send(AppenderAction::Start).unwrap();
        let appender = self.clone();

        let join_handle = std::thread::Builder::new()
            .name(identifier)
            .spawn(move || {
                let mut appender_type = into(appender.clone());

                loop {
                    let appender_action = appender_receiver.recv().unwrap();
                    match appender_action.clone() {
                        AppenderAction::Start => {
                            appender_type.start();
                        }
                        AppenderAction::Appenders { appenders } => {
                            appender_type.appenders(appenders);
                        }
                        AppenderAction::Append { content, flush } => {
                            appender_type.append(content, flush);
                        }
                        AppenderAction::Stop => {
                            appender_type.stop();
                            break;
                        }
                    }
                }
            })?;

        Ok(AppenderThread {
            join_handle: Some(join_handle),
            sender: Some(appender_sender),
        })
    }
}

#[derive(Debug)]
pub struct AppenderThread {
    join_handle: Option<JoinHandle<()>>,
    sender: Option<Sender<AppenderAction>>,
}

// Cloned AppenderThread does not have the join_handle
impl Clone for AppenderThread {
    fn clone(&self) -> AppenderThread {
        AppenderThread {
            join_handle: None,
            sender: self.sender.clone(),
        }
    }
}

impl AppenderThread {
    pub fn appenders(
        &self,
        appenders: HashMap<String, AppenderThread>,
    ) -> std::result::Result<(), Box<dyn Error>> {
        self.sender
            .as_ref()
            .unwrap()
            .send(AppenderAction::Appenders {
                appenders: appenders,
            })
            .unwrap();
        Ok(())
    }

    pub fn append(
        &self,
        content: AppenderContent,
        flush: bool,
    ) -> std::result::Result<(), Box<dyn Error>> {
        self.sender
            .as_ref()
            .unwrap()
            .send(AppenderAction::Append {
                content: content,
                flush: flush,
            })
            .unwrap();
        Ok(())
    }

    pub fn stop(&mut self) -> std::result::Result<(), Box<dyn Error>> {
        self.sender
            .as_ref()
            .unwrap()
            .send(AppenderAction::Stop)
            .unwrap();
        // TODO wait for the thread to finish
        //match &self.join_handle {
        //	Some(join_handle) => join_handle.join().unwrap(),
        //	None => (),
        //}
        //self.join_handle.unwrap().join().unwrap();
        self.join_handle.take().map(JoinHandle::join);
        //let ten_millis = std::time::Duration::from_millis(10000);
        //thread::sleep(ten_millis);
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub enum AppenderAction {
    Start,
    Appenders {
        appenders: HashMap<String, AppenderThread>,
    },
    Append {
        content: AppenderContent,
        flush: bool,
    },
    Stop,
}

#[derive(Debug, Clone)]
pub enum AppenderContent {
    Binary {
        data: Vec<u8>,
    },
    String {
        data: String,
    },
    Array {
        // Record
        leading: Option<String>,
        trailing: Option<String>,
        separator: Option<String>,
        values: Vec<String>,
    },
}

#[cfg(test)]
mod tests {
    //use super::*;
}
