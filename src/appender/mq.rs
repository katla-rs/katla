// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use jni::objects::JValueGen;
use jni::sys::JNIEnv;
use jni::{InitArgsBuilder, JNIVersion, JavaVM};

use crate::appender::{AppenderContent, AppenderType};
use crate::config::appender::Appender;
use std::borrow::Borrow;

/*
https://github.com/ibm-messaging/mq-dev-samples

https://www.ibm.com/docs/en/ibm-mq/9.3?topic=messaging-using-mq-classes-jms-sample-applications
https://github.com/ibm-messaging/mq-dev-patterns/blob/master/JMS/com/ibm/mq/samples/jms/JmsPut.java

https://repo1.maven.org/maven2/com/ibm/mq/com.ibm.mq.allclient/9.3.5.0/

And this web page describes how to install a container with a test IBM MQ installation
https://developer.ibm.com/tutorials/mq-connect-app-queue-manager-containers/
podman pull icr.io/ibm-messaging/mq:latest
podman images
podman volume create qm1data
podman run --env LICENSE=accept --env MQ_QMGR_NAME=QM1 --volume qm1data:/mnt/mqm --publish 1414:1414 --publish 9443:9443 --detach --env MQ_APP_PASSWORD=passw0rd --env MQ_ADMIN_PASSWORD=passw0rd --name QM1 icr.io/ibm-messaging/mq:latest
podman ps
podman exec -ti QM1 bash
*/
//#[derive(Debug)]
pub struct Mq {
    pub appender: Appender,
    pub jni_env: Option<JNIEnv>,
    pub jvm: Option<JavaVM>,
}

fn get_jvm() -> JavaVM {
    let jvm_args = InitArgsBuilder::new()
        .version(JNIVersion::V8)
        //.option("-Xcheck:jni")
        .option("-Djava.class.path=./java/lib/:./java/lib/com.ibm.mq.jakarta.client-9.3.5.0.jar:./java/lib/jakarta.jms-api-3.1.0.jar:./java/lib/jakarta.annotation-api-3.0.0.jar:./java/lib/json-20240303.jar")
        //.option("-verbose:class")
        .build()
        .unwrap();

    JavaVM::new(jvm_args).unwrap()
}

impl AppenderType for Mq {
    fn start(&mut self) {
        if let Appender::Mq {
            name,
            connection_string,
            queue_manager,
            channel,
            ccdt_url,
            queue,
            topic,
            user,
            password,
            application_name,
            cipher_suite,
            bindings,
        } = self.appender.clone()
        {
            self.jvm = Some(get_jvm());
            let mut guard = self.jvm.as_ref().unwrap().attach_current_thread().unwrap();
            let class_loader_class = guard.find_class("java/lang/ClassLoader").unwrap();
            let class_loader = guard
                .call_static_method(
                    class_loader_class,
                    "getSystemClassLoader",
                    "()Ljava/lang/ClassLoader;",
                    &[],
                )
                .unwrap();
            let thread_class = guard.find_class("java/lang/Thread").unwrap();
            let current_thread = guard
                .call_static_method(thread_class, "currentThread", "()Ljava/lang/Thread;", &[])
                .unwrap();
            if let JValueGen::Object(current_thread) = current_thread {
                guard
                    .call_method(
                        current_thread,
                        "setContextClassLoader",
                        "(Ljava/lang/ClassLoader;)V",
                        &[class_loader.borrow()],
                    )
                    .unwrap();
            }

            // Java Appender interface
            let appender = guard.find_class("rs/katla/appender/Appender").unwrap();
            let java_null_object = guard
                .get_static_field(appender.borrow(), "NULL_OBJECT", "Ljava/lang/Object;")
                .unwrap();

            let appender_type = guard.new_string("MQ").unwrap();
            let identifier = guard.new_string(name).unwrap();
            let mq_appender = guard
                .call_static_method(
                    appender.borrow(),
                    "getAppender",
                    "(Ljava/lang/String;Ljava/lang/String;)Lrs/katla/appender/Appender;",
                    &[
                        JValueGen::Object(&appender_type),
                        JValueGen::Object(&identifier),
                    ],
                )
                .unwrap();

            // Boolean.TRUE
            let boolean_class = guard.find_class("java/lang/Boolean").unwrap();
            let boolean_value_true = guard
                .get_static_field(boolean_class, "TRUE", "Ljava/lang/Boolean;")
                .unwrap();
            // Boolean.FALSE
            let boolean_class = guard.find_class("java/lang/Boolean").unwrap();
            let boolean_value_false = guard
                .get_static_field(boolean_class, "FALSE", "Ljava/lang/Boolean;")
                .unwrap();

            let java_util_hashmap_instance =
                guard.new_object("java/util/HashMap", "()V", &[]).unwrap();

            // application_name
            let application_name_key = guard.new_string("applicationName").unwrap();
            let application_name_value = guard.new_string(application_name.clone()).unwrap();
            guard
                .call_method(
                    java_util_hashmap_instance.borrow(),
                    "put",
                    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                    &[
                        JValueGen::Object(&application_name_key),
                        JValueGen::Object(&application_name_value),
                    ],
                )
                .unwrap();

            // bindings
            let bindings_key = guard.new_string("bindings").unwrap();
            //let bindings_value = guard.new_string(false).unwrap();
            guard
                .call_method(
                    java_util_hashmap_instance.borrow(),
                    "put",
                    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                    &[
                        JValueGen::Object(&bindings_key),
                        boolean_value_false.borrow(),
                    ],
                )
                .unwrap();

            // ccdt_url
            let ccdt_url_key = guard.new_string("ccdtUrl").unwrap();
            if let Some(ccdt_url) = ccdt_url.clone() {
                let ccdt_url_value = guard.new_string(ccdt_url.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&ccdt_url_key),
                            JValueGen::Object(&ccdt_url_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&ccdt_url_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            // channel
            let channel_key = guard.new_string("channel").unwrap();
            if let Some(channel) = channel.clone() {
                let channel_value = guard.new_string(channel.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&channel_key),
                            JValueGen::Object(&channel_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&channel_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            // cipher_suite
            let cipher_suite_key = guard.new_string("cipherSuite").unwrap();
            if let Some(cipher_suite) = cipher_suite.clone() {
                let cipher_suite_value = guard.new_string(cipher_suite.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&cipher_suite_key),
                            JValueGen::Object(&cipher_suite_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&cipher_suite_key),
                            java_null_object.borrow(),
                        ],
                    )
                    .unwrap();
            }

            // connection_string
            let connection_string_key = guard.new_string("connectionString").unwrap();
            let connection_string_value = guard.new_string(connection_string.clone()).unwrap();
            guard
                .call_method(
                    java_util_hashmap_instance.borrow(),
                    "put",
                    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                    &[
                        JValueGen::Object(&connection_string_key),
                        JValueGen::Object(&connection_string_value),
                    ],
                )
                .unwrap();

            // password
            let password_key = guard.new_string("password").unwrap();
            if let Some(password) = password.clone() {
                let password_value = guard.new_string(password.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&password_key),
                            JValueGen::Object(&password_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&password_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            // queue
            let queue_key = guard.new_string("queue").unwrap();
            if let Some(queue) = queue.clone() {
                let queue_value = guard.new_string(queue.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&queue_key),
                            JValueGen::Object(&queue_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&queue_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            // queue_manager
            let queue_manager_key = guard.new_string("queueManager").unwrap();
            let queue_manager_value = guard.new_string(queue_manager.clone()).unwrap();
            guard
                .call_method(
                    java_util_hashmap_instance.borrow(),
                    "put",
                    "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                    &[
                        JValueGen::Object(&queue_manager_key),
                        JValueGen::Object(&queue_manager_value),
                    ],
                )
                .unwrap();

            // topic
            let topic_key = guard.new_string("topic").unwrap();
            if let Some(topic) = topic.clone() {
                let topic_value = guard.new_string(topic.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[
                            JValueGen::Object(&topic_key),
                            JValueGen::Object(&topic_value),
                        ],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&topic_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            // user
            let user_key = guard.new_string("user").unwrap();
            if let Some(user) = user.clone() {
                let user_value = guard.new_string(user.clone()).unwrap();
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&user_key), JValueGen::Object(&user_value)],
                    )
                    .unwrap();
            } else {
                guard
                    .call_method(
                        java_util_hashmap_instance.borrow(),
                        "put",
                        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
                        &[JValueGen::Object(&user_key), java_null_object.borrow()],
                    )
                    .unwrap();
            }

            if let JValueGen::Object(mq_appender) = mq_appender {
                guard
                    .call_method(
                        mq_appender,
                        "start",
                        "(Ljava/util/HashMap;)V",
                        &[JValueGen::Object(&java_util_hashmap_instance.borrow())],
                    )
                    .unwrap();
            }
        }
    }

    fn appenders(&mut self, appenders: std::collections::HashMap<String, super::AppenderThread>) {}

    fn append(&mut self, content: AppenderContent, flush: bool) {
        let mut guard = self.jvm.as_mut().unwrap().attach_current_thread().unwrap();
        if let Appender::Mq { name, .. } = self.appender.clone() {
            // Java Appender interface
            let appender = guard.find_class("rs/katla/appender/Appender").unwrap();
            let java_null_object = guard
                .get_static_field(appender.borrow(), "NULL_OBJECT", "Ljava/lang/Object;")
                .unwrap();

            let appender_type = guard.new_string("MQ").unwrap();
            let identifier = guard.new_string(name).unwrap();
            let mq_appender = guard
                .call_static_method(
                    appender.borrow(),
                    "getAppender",
                    "(Ljava/lang/String;Ljava/lang/String;)Lrs/katla/appender/Appender;",
                    &[
                        JValueGen::Object(&appender_type),
                        JValueGen::Object(&identifier),
                    ],
                )
                .unwrap();

            if let JValueGen::Object(mq_appender) = mq_appender {
                match content {
                    AppenderContent::Binary { data } => {
                        let message = guard
                            .call_method(mq_appender.borrow(), "append", "([B)V", &[])
                            .unwrap();
                    }
                    AppenderContent::String { data } => {
                        let data = guard.new_string(data).unwrap();
                        let message = guard
                            .call_method(
                                mq_appender.borrow(),
                                "append",
                                "(Ljava/lang/String;)V",
                                &[JValueGen::Object(&data)],
                            )
                            .unwrap();
                    }
                    AppenderContent::Array {
                        leading,
                        trailing,
                        separator,
                        values,
                    } => {}
                }
            }
        }
    }

    fn stop(&mut self) {
        let mut guard = self.jvm.as_mut().unwrap().attach_current_thread().unwrap();

        if let Appender::Mq { name, .. } = self.appender.clone() {
            let appender = guard.find_class("rs/katla/appender/Appender").unwrap();
            let appender_type = guard.new_string("MQ").unwrap();
            let identifier = guard.new_string(name).unwrap();
            let mq_appender = guard
                .call_static_method(
                    appender,
                    "getAppender",
                    "(Ljava/lang/String;Ljava/lang/String;)Lrs/katla/appender/Appender;",
                    &[
                        JValueGen::Object(&appender_type),
                        JValueGen::Object(&identifier),
                    ],
                )
                .unwrap();
            if let JValueGen::Object(mq_appender) = mq_appender {
                guard.call_method(mq_appender, "stop", "()V", &[]).unwrap();
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[cfg(feature = "ibm-mq")]
    #[test]
    fn test_appender_type_mq_start() {
        let appender: Appender = ron::from_str(
            &"#![enable(implicit_some)](type:\"mq\",name:\"mq-name-test\",connectionString:\"localhost(1414)\",queueManager:\"QM1\",queue:\"DEV.QUEUE.1\",channel:\"DEV.APP.SVRCONN\",user:\"app\",password:\"passw0rd\",applicationName:\"App Name\",)",
        )
        .unwrap();
        let mut mq = Mq {
            appender: appender,
            jni_env: None,
            jvm: None,
        };
        mq.start();
        mq.append(
            AppenderContent::String {
                data: "blah blah".to_string(),
            },
            true,
        );
        mq.stop();
    }
}
