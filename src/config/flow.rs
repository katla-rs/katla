// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::appender::AppenderContent;
use crate::appender::AppenderThread;
use crate::model::context::Context;

use super::super::model::entity::Entity;
use super::entry::EntryThread;
use super::entry::EntryAction;
use super::feed::Feed;
use super::feed::Pagination;
use super::Identifier;
use serde::{Deserialize, Serialize};
use super::entity_provider::EntityProvider;
use super::time::interval::DeviatedInterval;
use super::time::duration::ReferencedDeviatedDuration;

use std::time::Instant;
use std::time::SystemTime;
use std::time::Duration;
use std::collections::HashMap;
//use regex::Regex;
use std::error::Error;
use std::{
	sync::mpsc::{channel, Receiver, Sender},
//	thread,
	thread::JoinHandle,
};
use std::collections::BTreeMap;

use std::rc::Rc;
use std::thread;
use crate::eval::template as template_execute;
use crate::serde::util::option_string_or_map;

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Flow {
	pub name: String,
	#[serde(default)] //  This is needed for the optional to work
	#[serde(deserialize_with = "option_string_or_map")]
	#[serde(rename = "timeInterval")]
	pub time_interval: Option<DeviatedInterval>,
	pub entries: Vec<String>,
	pub outputs: Vec<FlowOutput>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct FlowOutput {
	#[serde(default)] //  This is needed for the optional to work
	#[serde(deserialize_with = "option_string_or_map")]
	pub duration: Option<ReferencedDeviatedDuration>,
	pub feed: String,
	pub appender: String,
}

impl Flow {

	pub fn start(&mut self, result_sender: Sender<FlowStat>, entity_providers: HashMap<String, EntityProvider>, entry_threads: HashMap<String, EntryThread>, feeds: HashMap<String, Feed>, appender_threads: HashMap<String, AppenderThread>) -> Result<FlowThread, Box<dyn Error>> {
		let identifier = self.get_identifier();
		let (flow_sender, flow_receiver): (Sender<FlowAction>, Receiver<FlowAction>) = channel();
		flow_sender.send(FlowAction::Start { result_sender: result_sender, entity_providers: entity_providers, entry_threads: entry_threads, feeds: feeds, appender_threads: appender_threads }).unwrap();
		let flow = self.clone();

		let join_handle = std::thread::Builder::new().name(identifier).spawn(move || {
			let mut entry_threads_local: HashMap<String, EntryThread>;
			let mut feeds_local: HashMap<String, Feed>;
			let mut appender_threads_local: HashMap<String, AppenderThread>;
			let mut context = Context::application();

			//let running = false;
			loop {
				let flow_action = flow_receiver.recv().unwrap();
				match flow_action.clone() {
					FlowAction::Start { result_sender, entity_providers, entry_threads, feeds, appender_threads } => {
						entry_threads_local = entry_threads;
						feeds_local = feeds;
						appender_threads_local = appender_threads;

						// gathering all needed timing information like start, end repetitions, ...
						let repetitions = flow.clone().time_interval.map_or(None, |t| t.interval).map_or(None, |i| i.repetitions);
						let start = match flow.clone().time_interval {
							Some(time_interval) => time_interval.interval.map_or(None, |i| i.start).map_or_else(|| SystemTime::now(), |t| SystemTime::from(t)),
							None => SystemTime::now(),
						};
						let mut last_repetition_time = SystemTime::UNIX_EPOCH;
						let duration = flow.clone().time_interval.map_or(None, |t| t.interval).map_or(None, |i| i.duration.map_or(None, |d| Some(Duration::from(d))));
						let end = match flow.clone().time_interval {
							Some(time_interval) => time_interval.interval.map_or(None, |i| i.end).map_or(None, |t| Some(SystemTime::from(t))),
							None => None,
						};
						let _deviation = match flow.clone().time_interval {
							Some(time_interval) => time_interval.deviation.unwrap_or_else(|| ("".to_string(), 0)),
							None => ("".to_string(), 0),
						};





						// stats
						let total = match repetitions {
							Some(repetitions) => repetitions,
							None => std::u64::MAX,
						};
						let start_instant = Instant::now();
						let mut update_instant = start_instant;
						result_sender.send(FlowStat { flow_identifier: flow.get_identifier(), start_instant: start_instant, update_instant: update_instant, total: total, done: 0 }).unwrap_or_else(|_| ());





						// feeding (page) headers
						for flow_output in &flow.outputs {
							match feeds_local.get(&flow_output.feed).unwrap() {

								Feed::Csv { header, leading, trailing, delimiter, .. } => {
									let mut csv_columns = Vec::new();

									for csv_column in header {
										let csv_value = template_execute(&mut context, csv_column.clone()).unwrap();
										csv_columns.push(csv_value.to_string());
									}
									// appending
									appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::Array { leading: leading.clone(), trailing: trailing.clone(), separator: delimiter.clone(), values: csv_columns.clone() }, true).expect("Unable to append data");
								},

								Feed::File { name: _, header, page_header, content: _, footer: _, page_footer: _, pagination: _ } => {
									match header.clone() {
										Some(header) => {
											let value = template_execute(&mut context, header).unwrap();
											// appending
											appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
										},
										None => (),
									}
									if total > 0 {
										match page_header.clone() {
											Some(page_header) => {
												let value = template_execute(&mut context, page_header).unwrap();
												// appending
												appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
											},
											None => (),
										}
									}
								},

								Feed::Json { name: _, .. } => {
								},
							}
						}





						// sleep until it is time to 'start'
						loop {
							let sleep_duration = start.duration_since(SystemTime::now());
							if sleep_duration.clone().map_or(0, |d| d.as_nanos()) == 0 { break; }
							// TODO only sleep for xx seconds so we can log we are still alive
							//thread::sleep(Duration::from_secs(60));
							thread::sleep(sleep_duration.unwrap());
						}

						let mut repetition = 0;
						let mut schedules :BTreeMap<SystemTime, Vec<Rc<(&FlowOutput, Context)>>> = BTreeMap::new();
						while match end {
								// end timestamp defined but if there are still some scheduled outputs do process them
								Some(end) => SystemTime::now() <= end || schedules.len() > 0,
								None => {
									match repetitions {
										// no end timestamp but number of repetitions defined, so run until number of repetitions is reached or still some scheduled outputs
										Some(repetitions) => repetition < repetitions || schedules.len() > 0,
										// no end timestamp and no number of repetitions defined, so run forever or until we receive a stop signal
										// TODO receive a stop signal
										None => true,
									}
								},
							}
						{
							// check if we need to start a new repetition
							if match duration {
									Some(duration) => (last_repetition_time + duration) < SystemTime::now() && repetition < repetitions.map_or(std::u64::MAX, |r| r) && SystemTime::now() <= end.map_or_else(|| SystemTime::now(), |e| e),
									// no duration set so always start a new repetition
									None => true,
								}
							{
								// new repetition
								last_repetition_time = SystemTime::now();
								repetition += 1;

								let mut repetition_context = Context::local();
								for entry_name in &flow.entries {
									let entry_thread = entry_threads_local.get(entry_name).unwrap();
									let (result_sender, result_receiver): (Sender<Entity>, Receiver<Entity>) = channel();
									entry_thread.sender.as_ref().unwrap().send(EntryAction::Generate { result_sender: result_sender, entity_providers: entity_providers.clone() }).unwrap();
									repetition_context.insert(Some(entry_name.to_string()), result_receiver.recv().unwrap());
									//println!("{:#?}", repetition_context);
								}
	
								// populate schedules with info when to fire flow outputs
								for flow_output in &flow.outputs {
									// TODO calcuate correct timestamp base on duration and reference
									//println!("DURATION {:#?}", flow_output.duration.clone());
									let schedule_timestamp = SystemTime::now() + flow_output.duration.clone().map_or_else(|| Duration::from_secs(0), |d| Duration::from(d));
									let mut schedule = schedules.get_mut(&schedule_timestamp).map_or(Vec::new(), |s| s.to_vec());
									schedule.push(Rc::new((flow_output, repetition_context.clone())));
									schedules.insert(schedule_timestamp, schedule);
								}
							}





							// stats
							if update_instant.elapsed().as_secs() > 1 {
								update_instant = Instant::now();
								result_sender.send(FlowStat { flow_identifier: flow.get_identifier(), start_instant: start_instant, update_instant: update_instant, total: total, done: repetition }).unwrap_or_else(|_| ());
							}





							//println!("XXXXXXXX R {:#?}", repetition);

							//println!("XXXXXXXX ss {:#?}", schedules);
							let futures = schedules.split_off(&SystemTime::now());
							for (_schedule_key, schedule_values) in schedules {










								//println!("XXXXXXXX S {:#?}", schedule);
								// todo process scheduled flow outputs
								for schedule_value in schedule_values {
									//let (flow_output, context) = Rc::get_mut(&mut schedule_value.clone()).unwrap();
									let (flow_output, mut context) = Rc::try_unwrap(schedule_value).unwrap();
									//let flow_output = schedule_value.0;
									//let context = schedule_value.1;
									let feed = feeds_local.get(&flow_output.feed).unwrap();
									match feed {

										Feed::Csv { record, leading, trailing, delimiter, .. } => {
											let mut csv_columns = Vec::new();
					
											for csv_column in record {
												let csv_value = template_execute(&mut context, csv_column.clone()).unwrap();
												csv_columns.push(csv_value.to_string());
											}
											// appending
											appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::Array { leading: leading.clone(), trailing: trailing.clone(), separator: delimiter.clone(), values: csv_columns.clone() }, true).expect("Unable to append data");
										},
					
										Feed::File { name: _, header: _, page_header, content, footer: _, page_footer, pagination} => {
											match content.clone() {
												Some(content) => {
													let value = template_execute(&mut context, content).unwrap();
													// appending
													appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
												},
												None => (),
											}
					
											match pagination {
												Pagination::InFile { count: page_count } => {
													// TODO either get repetition from schedules or move it to feed threads
													if repetition % page_count == 0 {
														match page_footer.clone() {
															Some(page_footer) => {
																let value = template_execute(&mut context, page_footer).unwrap();
																// appending
																appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
															},
															None => (),
														}
					
														// TODO either get repetition from schedules or move it to feed threads
														if repetition < total {
															match page_header.clone() {
																Some(page_header) => {
																	let value = template_execute(&mut context, page_header).unwrap();
																	// appending
																	appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
																},
																None => (),
															}
														}
														}
												},
												Pagination::PerFile { count: _ } => {
												},
											}
										},

										Feed::Json { name: _, .. } => {
											appender_threads_local.get_mut(&flow_output.appender).unwrap().append(feed.clone().to_appender_content(&context).unwrap(), true).expect("Unable to append data");
										},
									}
								}
				









							}

							schedules = futures;
							// get first schedule timestamp
							let schedule_timestamps: Vec<_> = schedules.keys().cloned().collect();
							match duration {
								Some(duration) => {
									if (schedule_timestamps.len() > 0) && schedule_timestamps[0] < (last_repetition_time + duration) {
										// sleep until next schedule timestamp
										loop {
											let sleep_duration = schedule_timestamps[0].duration_since(SystemTime::now());
											if sleep_duration.clone().map_or(0, |d| d.as_nanos()) == 0 { break; }
											// TODO only sleep for xx seconds so we can log we are still alive
											//thread::sleep(Duration::from_secs(60));
											thread::sleep(sleep_duration.unwrap());
										}
									} else {
										// no schedules, sleep until next repetition
										loop {
											let sleep_duration = (last_repetition_time + duration).duration_since(SystemTime::now());
											if sleep_duration.clone().map_or(0, |d| d.as_nanos()) == 0 { break; }
											// TODO only sleep for xx seconds so we can log we are still alive
											//thread::sleep(Duration::from_secs(60));
											thread::sleep(sleep_duration.unwrap());
										}
									}
								},
								// no duration so lets do the next repetition
								None => (),
							}
						}




						// stats
						update_instant = Instant::now();
						result_sender.send(FlowStat { flow_identifier: flow.get_identifier(), start_instant: start_instant, update_instant: update_instant, total: total, done: total }).unwrap_or_else(|_| ());




						// feeding (page) footers
						for flow_output in &flow.outputs {
							match feeds_local.get(&flow_output.feed).unwrap() {
								Feed::Csv { .. } => {
								},
		
								Feed::File { name: _, header: _, page_header: _, content: _, footer, page_footer, pagination} => {
									match footer.clone() {
										Some(footer) => {
											let value = template_execute(&mut context, footer).unwrap();
											// appending
											appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
										},
										None => (),
									}

									match pagination {
										Pagination::InFile { count: page_count } => {
											if total % page_count != 0 {
												match page_footer.clone() {
													Some(page_footer) => {
														let value = template_execute(&mut context, page_footer).unwrap();
														// appending
														appender_threads_local.get_mut(&flow_output.appender).unwrap().append(AppenderContent::String { data: value.to_string() }, true).expect("Unable to append data");
													},
													None => (),
												}
											}
										},
										Pagination::PerFile { count: _ } => {
										},
									}
								},

								Feed::Json { name: _, .. } => {
								},
							}
						}
































						for _count in 0..total {
							// rhai engine scope
							//let mut scope = Scope::new();
							// TODO check if rhai engine is used, otherwise an empty scope is fine
							//for (entity_name, entity) in entities.iter() {
								//scope.set_value(entity_name.as_str(), entity.clone());
							//}
						

							for entry_name in &flow.entries {
								let entry_thread = entry_threads_local.get(entry_name).unwrap();
								let (result_sender, result_receiver): (Sender<Entity>, Receiver<Entity>) = channel();
								entry_thread.sender.as_ref().unwrap().send(EntryAction::Generate { result_sender: result_sender, entity_providers: entity_providers.clone() }).unwrap();
								context.insert(Some(entry_name.to_string()), result_receiver.recv().unwrap());
			//println!("{:#?}", entities);
							}

						}

					},

					FlowAction::Stop => {
						break;
					},
				}
			}
		})?;

		Ok(FlowThread {
			join_handle: Some(join_handle),
			sender: Some(flow_sender),
		})
	}

	pub fn add_flow_output(&mut self, flow_output: FlowOutput) {
		self.outputs.push(flow_output);
	}

}

impl Identifier for Flow {
	type Value = String;

	fn get_identifier(&self) -> String {
		self.name.to_string()
	}
}

#[derive(Debug)]
pub struct FlowThread {
	join_handle: Option<JoinHandle<()>>,
	sender: Option<Sender<FlowAction>>,
}

// Cloned FlowThread does not have the join_handle
impl Clone for FlowThread {
	fn clone(&self) -> FlowThread {
		FlowThread {
			join_handle: None,
			sender: self.sender.clone(),
		}
	}
}

impl FlowThread {
	pub fn stop(&mut self) -> Result<(), Box<dyn Error>> {
		self.sender.as_ref().unwrap().send(FlowAction::Stop).unwrap();
		self.join_handle.take().map(JoinHandle::join);
		Ok(())
	}
}

#[derive(Debug, Clone)]
pub enum FlowAction {
	Start {
		result_sender: Sender<FlowStat>,
		entity_providers: HashMap<String, EntityProvider>,
		entry_threads: HashMap<String, EntryThread>,
		feeds: HashMap<String, Feed>,
		appender_threads: HashMap<String, AppenderThread>,
	},
	Stop,
}

#[derive(Debug, Clone)]
pub struct FlowStat {
	pub flow_identifier: String,
	pub start_instant: Instant,
	pub update_instant: Instant,
	pub total: u64,
	pub done: u64,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_flow_deserialize_time_interval() {
        // JSON
        let flow: Flow = serde_json::from_str(&"{\"name\":\"name-test\",\"timeInterval\":\"R2/2021-03-14T13:07:35.456Z/PT0.0001S\",\"entries\":[],\"outputs\":[]}").unwrap();
        assert_eq!(flow.name, "name-test");

		// RON
		// TODO get rid off the Some()
        let flow: Flow = ron::from_str(&"#![enable(implicit_some)](name:\"name-test\",timeInterval:\"R2/2021-03-14T13:07:35.456Z/PT0.0001S\",entries:[],outputs:[],)").unwrap();
        assert_eq!(flow.name, "name-test");
    }

    #[test]
    fn test_flow_output_deserialize_duration() {
		// JSON
        let flow_output: FlowOutput = serde_json::from_str(&"{\"duration\":\"PT4S\",\"feed\":\"feed-test\",\"appender\":\"appender-test\"}").unwrap();
        assert_eq!(flow_output.duration, Some(ReferencedDeviatedDuration::from("PT4S")));
        assert_eq!(flow_output.feed, "feed-test");
        assert_eq!(flow_output.appender, "appender-test");

		// RON
        let flow_output: FlowOutput = ron::from_str(&"#![enable(implicit_some)](duration:\"PT4S\",feed:\"feed-test\",appender:\"appender-test\",)").unwrap();
        assert_eq!(flow_output.duration, Some(ReferencedDeviatedDuration::from("PT4S")));
        assert_eq!(flow_output.feed, "feed-test");
        assert_eq!(flow_output.appender, "appender-test");
    }

}
