// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

// katla::config::time::duration

use crate::serde::error::SerDeError;

use super::deviation::Deviation;
use super::reference::Reference;
use core::ops::Add;
use fraction::Fraction;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use std::time::Duration as StdTimeDuration;
use std::time::SystemTime;

// largely based on ISO 8601 Duration
/*
 * https://en.wikipedia.org/wiki/ISO_8601#Durations
 *
 * PnYnMnDTnHnMnS
 * PnW
 * P<date>T<time>
 */
#[derive(Deserialize, Serialize, Debug, Default, Clone, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Duration {
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    weeks: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    years: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    months: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    days: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    hours: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    minutes: Option<Fraction>,
    #[serde(default)] //  This is needed for the optional to work
    #[serde(with = "serde_fraction")]
    seconds: Option<Fraction>,
}

impl FromStr for Duration {
    type Err = SerDeError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let mut weeks = None;
        let mut years = None;
        let mut months = None;
        let mut days = None;
        let mut hours = None;
        let mut minutes = None;
        let mut seconds = None;

        lazy_static! {
            static ref DURATION_REGEX: Regex = Regex::new(r"^P(?:(?P<weeks>(?:\d+)|(?:\d+[.,]\d+))W$)?(?P<years>(?:\d+Y)|(?:\d+[.,]\d+Y$))?(?P<months>(?:\d+M)|(?:\d+[.,]\d+M$))?(?P<days>(?:\d+D)|(?:\d+[.,]\d+D$))?(T(?P<hours>(?:\d+H)|(?:\d+[.,]\d+H$))?(?P<minutes>(?:\d+M)|(?:\d+[.,]\d+M$))?(?P<seconds>\d+([.,]\d+)?S)?)??$").unwrap();
        }
        let captures = DURATION_REGEX.captures(string);
        match captures {
            Some(captures) => {
                match captures.name("weeks") {
                    Some(weeks_capture) => {
                        let capture = weeks_capture.as_str().replace(",", ".");
                        weeks = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("years") {
                    Some(years_capture) => {
                        let mut capture = years_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        years = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("months") {
                    Some(months_capture) => {
                        let mut capture = months_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        months = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("days") {
                    Some(days_capture) => {
                        let mut capture = days_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        days = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("hours") {
                    Some(hours_capture) => {
                        let mut capture = hours_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        hours = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("minutes") {
                    Some(minutes_capture) => {
                        let mut capture = minutes_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        minutes = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
                match captures.name("seconds") {
                    Some(seconds_capture) => {
                        let mut capture = seconds_capture.as_str();
                        capture = &capture[0..(capture.len() - 1)];
                        let capture = capture.replace(",", ".");
                        seconds = Some(Fraction::from_str(&capture).unwrap());
                    }
                    None => (),
                };
            }
            None => (),
        }

        Ok(Duration {
            weeks: weeks,
            years: years,
            months: months,
            days: days,
            hours: hours,
            minutes: minutes,
            seconds: seconds,
        })
    }
}

impl From<&str> for Duration {
    fn from(string: &str) -> Self {
        Duration::from_str(string).unwrap()
    }
}

impl From<Duration> for StdTimeDuration {
    fn from(duration: Duration) -> Self {
        let (seconds, nanos) = duration.get_inaccurate_seconds_and_nanos();
        StdTimeDuration::new(seconds, nanos)
    }
}

impl Duration {
    pub fn get_inaccurate_seconds_and_nanos(&self) -> (u64, u32) {
        let mut seconds_and_nanos = Fraction::from(0.0);
        let multiplier = Fraction::from(1.0);
        match self.seconds {
            Some(seconds) => seconds_and_nanos = seconds_and_nanos + (seconds * multiplier),
            None => (),
        }
        let multiplier = multiplier * Fraction::from(60);
        match self.minutes {
            Some(minutes) => seconds_and_nanos = seconds_and_nanos + (minutes * multiplier),
            None => (),
        }
        let multiplier = multiplier * Fraction::from(60);
        match self.hours {
            Some(hours) => seconds_and_nanos = seconds_and_nanos + (hours * multiplier),
            None => (),
        }
        let multiplier = multiplier * Fraction::from(24);
        match self.days {
            Some(days) => seconds_and_nanos = seconds_and_nanos + (days * multiplier),
            None => (),
        }
        let multiplier = multiplier * Fraction::from(7);
        match self.weeks {
            Some(weeks) => seconds_and_nanos = seconds_and_nanos + (weeks * multiplier),
            None => (),
        }
        //let multiplier = multiplier * Fraction::from(4.2857);
        let multiplier = multiplier * Fraction::from(4);
        match self.months {
            Some(months) => seconds_and_nanos = seconds_and_nanos + (months * multiplier),
            None => (),
        }
        let multiplier = multiplier * Fraction::from(12);
        match self.years {
            Some(years) => seconds_and_nanos = seconds_and_nanos + (years * multiplier),
            None => (),
        }
        let seconds = format!("{:.0}", seconds_and_nanos).parse::<u64>().unwrap();
        let nanos_fraction =
            (seconds_and_nanos - Fraction::from(seconds)) * Fraction::from(1000000000);
        let nanos = format!("{:.0}", nanos_fraction).parse::<u32>().unwrap();
        (seconds, nanos)
    }
}

#[derive(Deserialize, Serialize, Debug, Default, Clone, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ReferencedDeviatedDuration {
    #[serde(deserialize_with = "super::super::super::serde::util::option_string_or_map")]
    duration: Option<Duration>,
    deviation: Option<Deviation>,
    reference: Option<Reference>,
}

impl FromStr for ReferencedDeviatedDuration {
    type Err = SerDeError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let parts = string.rsplitn(2, "/X").collect::<Vec<&str>>();
        let mut deviation = None;
        let reference = None;

        let duration = Some(Duration::from(parts[0]));
        if parts.len() == 2 {
            deviation = Some((parts[1].to_string(), 0));
        }

        Ok(ReferencedDeviatedDuration {
            duration: duration,
            deviation: deviation,
            reference: reference,
        })
    }
}

impl From<&str> for ReferencedDeviatedDuration {
    fn from(string: &str) -> Self {
        ReferencedDeviatedDuration::from_str(string).unwrap()
    }
}

impl From<ReferencedDeviatedDuration> for StdTimeDuration {
    fn from(duration: ReferencedDeviatedDuration) -> Self {
        duration.duration.map_or_else(
            || StdTimeDuration::from_secs(0),
            |d| StdTimeDuration::from(d),
        )
    }
}

impl Add<ReferencedDeviatedDuration> for SystemTime {
    type Output = SystemTime;

    fn add(self, duration: ReferencedDeviatedDuration) -> SystemTime {
        self + StdTimeDuration::from(duration)
    }
}

mod serde_fraction {
    use std::str::FromStr;
    use fraction::Decimal;
    use fraction::Fraction;
    use serde::{de::Error, Deserialize, Deserializer, Serialize, Serializer};

    pub fn serialize<S: Serializer>(
        fraction: &Option<Fraction>,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        match fraction {
            Some(fraction) => Decimal::from_fraction(*fraction)
                .to_string()
                .serialize(serializer),
            //None => serializer.serialize_none(),
            None => "".serialize(serializer),
        }
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Option<Fraction>, D::Error> {
        let mut fraction: String = Deserialize::deserialize(deserializer)?;
        fraction = fraction.replace(",", ".");
        let fraction_str: &str = &fraction;
        match fraction_str {
            "" => Ok(None),
            _ => Ok(Some(
                Fraction::from_str(&fraction).map_err(D::Error::custom)?,
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    #![allow(non_snake_case)]
    use super::*;

    /*
    P1Y
    P1.4Y
    P1.4Y2M
    P14Y2.5M
    P14Y2.5MT1M
    P2MT30M
    P2MT30.5M
    P2MT30.5M3S
    PT6H
    PT6.5H
    PT6.5H12S

    P3Y29DT4H35M59S
    P1Y1M1DT2H12M34.23S
    P0001-12-12T12:34:56
    P00011212T123456
    */

    #[test]
    fn test_duration_from_P5W() {
        let duration = Duration::from("P5W");
        assert_eq!(duration.weeks, Some(Fraction::from(5)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P5W() {
        // JSON
        assert_eq!(serde_json::to_string(&Duration::from("P5W")).unwrap(), "{\"weeks\":\"5\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
        // RON
        assert_eq!(ron::to_string(&Duration::from("P5W")).unwrap(), "(weeks:\"5\",years:\"\",months:\"\",days:\"\",hours:\"\",minutes:\"\",seconds:\"\")");
        }

    #[test]
    fn test_duration_deserialize_P5W() {
        // JSON
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // RON
        let duration: Duration = ron::from_str(&"(weeks:\"5\",years:\"\",months:\"\",days:\"\",hours:\"\",minutes:\"\",seconds:\"\")").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = ron::from_str(&"(weeks:\"5\")").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P5p2W() {
        let duration = Duration::from("P5.2W");
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P5p2W() {
        assert_eq!(serde_json::to_string(&Duration::from("P5.2W")).unwrap(), "{\"weeks\":\"5.2\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_P5p2W() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5.2\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5.2\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P5c2W() {
        let duration = Duration::from("P5,2W");
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P5c2W() {
        assert_eq!(serde_json::to_string(&Duration::from("P5,2W")).unwrap(), "{\"weeks\":\"5.2\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_P5c2W() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5.2\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"5.2\"}").unwrap();
        assert_eq!(duration.weeks, Some(Fraction::from(5.2)));
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_Pm5p2W() {
        let duration = Duration::from("P-5.2W");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_Pm5p2W() {
        assert_eq!(serde_json::to_string(&Duration::from("P-5.2W")).unwrap(), "{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_Pm5p2W() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P5W1D() {
        let duration = Duration::from("P5W1D");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P5W1D() {
        assert_eq!(serde_json::to_string(&Duration::from("P5W1D")).unwrap(), "{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_P5W1D() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P() {
        let duration = Duration::from("P");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P() {
        assert_eq!(serde_json::to_string(&Duration::from("P")).unwrap(), "{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_P() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_PT() {
        let duration = Duration::from("PT");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_PT() {
        assert_eq!(serde_json::to_string(&Duration::from("PT")).unwrap(), "{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_PT() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"\",\"months\":\"\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, None);
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P3MT() {
        let duration = Duration::from("P3MT");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, Some(Fraction::from(3)));
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_serialize_P3MT() {
        assert_eq!(serde_json::to_string(&Duration::from("P3MT")).unwrap(), "{\"weeks\":\"\",\"years\":\"\",\"months\":\"3\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}");
    }

    #[test]
    fn test_duration_deserialize_P3MT() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"\",\"months\":\"3\",\"days\":\"\",\"hours\":\"\",\"minutes\":\"\",\"seconds\":\"\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, Some(Fraction::from(3)));
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);

        // optionals
        let duration: Duration = serde_json::from_str(&"{\"months\":\"3\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, None);
        assert_eq!(duration.months, Some(Fraction::from(3)));
        assert_eq!(duration.days, None);
        assert_eq!(duration.hours, None);
        assert_eq!(duration.minutes, None);
        assert_eq!(duration.seconds, None);
    }

    #[test]
    fn test_duration_from_P1Y2M10DT2H30M5S() {
        let duration = Duration::from("P1Y2M10DT2H30M5S");
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, Some(Fraction::from(1)));
        assert_eq!(duration.months, Some(Fraction::from(2)));
        assert_eq!(duration.days, Some(Fraction::from(10)));
        assert_eq!(duration.hours, Some(Fraction::from(2)));
        assert_eq!(duration.minutes, Some(Fraction::from(30)));
        assert_eq!(duration.seconds, Some(Fraction::from(5)));
    }

    #[test]
    fn test_duration_serialize_P1Y2M10DT2H30M5S() {
        assert_eq!(serde_json::to_string(&Duration::from("P1Y2M10DT2H30M5S")).unwrap(), "{\"weeks\":\"\",\"years\":\"1\",\"months\":\"2\",\"days\":\"10\",\"hours\":\"2\",\"minutes\":\"30\",\"seconds\":\"5\"}");
    }

    #[test]
    fn test_duration_deserialize_P1Y2M10DT2H30M5S() {
        let duration: Duration = serde_json::from_str(&"{\"weeks\":\"\",\"years\":\"1\",\"months\":\"2\",\"days\":\"10\",\"hours\":\"2\",\"minutes\":\"30\",\"seconds\":\"5\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, Some(Fraction::from(1)));
        assert_eq!(duration.months, Some(Fraction::from(2)));
        assert_eq!(duration.days, Some(Fraction::from(10)));
        assert_eq!(duration.hours, Some(Fraction::from(2)));
        assert_eq!(duration.minutes, Some(Fraction::from(30)));
        assert_eq!(duration.seconds, Some(Fraction::from(5)));

        // optional weeks
        let duration: Duration = serde_json::from_str(&"{\"years\":\"1\",\"months\":\"2\",\"days\":\"10\",\"hours\":\"2\",\"minutes\":\"30\",\"seconds\":\"5\"}").unwrap();
        assert_eq!(duration.weeks, None);
        assert_eq!(duration.years, Some(Fraction::from(1)));
        assert_eq!(duration.months, Some(Fraction::from(2)));
        assert_eq!(duration.days, Some(Fraction::from(10)));
        assert_eq!(duration.hours, Some(Fraction::from(2)));
        assert_eq!(duration.minutes, Some(Fraction::from(30)));
        assert_eq!(duration.seconds, Some(Fraction::from(5)));
    }
}
