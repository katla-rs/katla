// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

// katla::csv::util

use std::error::Error;

// TODO make it crate only
pub/*(crate)*/ fn convert_spread_sheet_ranges(sheet_names: &Vec<String>, ranges: &Vec<String>) -> Result<Vec<Vec<(u32, i32, i32, i32, i32)>>, Box<dyn Error>> {
	debug!("sheet names: {:#?} and ranges = {:#?}", sheet_names, ranges);
	let mut converted_ranges = Vec::new();

	for range in ranges {
		converted_ranges.push(convert_spread_sheet_range(sheet_names, range).unwrap());
	}

	Ok(converted_ranges)
}

// TODO make it crate only
pub/*(crate)*/ fn convert_spread_sheet_range(sheet_names: &Vec<String>, range: &String) -> Result<Vec<(u32, i32, i32, i32, i32)>, Box<dyn Error>> {
	debug!("sheet names: {:#?} and range = {:#?}", sheet_names, range);
	let mut ranges = Vec::new();

	// split into single ranges, multiple ranges are seperated by ','
	for single_range in range.split(",") {
		// defaulting sheet to the first one
		let mut sheet_index = 0 as u32;
		let range_only;
		let sheet_split: Vec<&str> = single_range.split(".").collect();
		if sheet_split.len() > 1 {
			match sheet_names.iter().position(|x| x == sheet_split[0]) {
				Some(index) => sheet_index = index as u32,
				None => {
					sheet_index = sheet_split[0].parse().unwrap();
					sheet_index -= 1;
				},
			}
			range_only = String::from(*sheet_split.get(1).unwrap());
		} else {
			range_only = String::from(*sheet_split.get(0).unwrap());
		}

		let mut upper_col: i32;
		let mut upper_row: i32;
		let lower_col: i32;
		let lower_row: i32;

		let range_split: Vec<&str> = range_only.split(":").collect();
		let upper_left = convert_spread_sheet_range_part(&range_split[0].to_string()).unwrap();
		upper_row = upper_left.0;
		upper_col = upper_left.1;
		if range_split.len() > 1 && range_split[1].len() > 0 {
			let lower_right = convert_spread_sheet_range_part(&range_split[1].to_string()).unwrap();
			lower_row = lower_right.0;
			lower_col = lower_right.1;
			if upper_row == -1 {
				upper_row = 0;
			}
			if upper_col == -1 {
				upper_col = 0;
			}
		} else {
			lower_row = upper_row;
			lower_col = upper_col;

			if upper_row == -1 {
				upper_row = 0;
			}
			if upper_col == -1 {
				upper_col = 0;
			}
		}

		ranges.push((sheet_index, upper_row, upper_col, lower_row, lower_col));
	}

	Ok(ranges)
}

// convert ie 
// C -> 0,2
// C4 -> 3,2
// ABC123 -> 122,626
pub(crate) fn convert_spread_sheet_range_part(range_part: &String) -> Result<(i32, i32), Box<dyn Error>> {
	let mut row = 0  as i32;
	let mut col = 0  as i32;
	if range_part.chars().all(char::is_numeric) {
		col = -1;
		row = range_part.parse().unwrap();
		row -= 1;
	} else {
		let mut has_row = false;
		for character in range_part.chars() {
			if !character.is_digit(10) {
				col *= 24;
				let value = character as i32 - 'A' as i32;
				col += value;
				col += 1;
			} else {
				has_row = true;
				row *= 10;
				let value = character as i32 - '0' as i32;
				row += value;
			}
		}
		col -= 1;
		if !has_row {
			row = -1;
		} else {
			row -= 1;
		}
	}
	Ok((row, col))
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_convert_spread_sheet_range_part() {
		assert_eq!(convert_spread_sheet_range_part(&"C".to_string()).unwrap(), (-1, 2));
		assert_eq!(convert_spread_sheet_range_part(&"C1".to_string()).unwrap(), (0, 2));
		assert_eq!(convert_spread_sheet_range_part(&"C4".to_string()).unwrap(), (3, 2));
		assert_eq!(convert_spread_sheet_range_part(&"A4".to_string()).unwrap(), (3, 0));
		assert_eq!(convert_spread_sheet_range_part(&"4".to_string()).unwrap(), (3, -1));
		assert_eq!(convert_spread_sheet_range_part(&"ABC123".to_string()).unwrap(), (122, 626));
	}

	#[test]
	fn test_convert_spread_sheet_ranges() {
		let mut sheets = Vec::new();
		sheets.push("FirstSheetName".to_string());
		sheets.push("SecondSheetName".to_string());
		sheets.push("ThirdSheetName".to_string());
		sheets.push("FourthSheetName".to_string());
		sheets.push("FifthSheetName".to_string());

		let mut ranges = Vec::new();
		ranges.push("C:".to_string()); // column C
		ranges.push("H25:H".to_string()); // column H from row 25 till the end
		ranges.push("L25:L37".to_string()); // column L from row 25 till row 37 included
		ranges.push("SecondSheetName.L103:L135".to_string()); // column L from row 25 till row 37 included from sheet with name SecondSheetName
		ranges.push("3.L103:L135".to_string()); // column L from row 103 till row 135 included from the third sheet
		ranges.push("T:T49".to_string()); // column T from the beginning till row 49 included
		ranges.push("ABC33:ABC59".to_string()); // column ABC from row 33 till row 59 included
		ranges.push("25:".to_string()); // row 25
		ranges.push("C25:25".to_string()); // row 25 from column C till the last column
		ranges.push("C25:H25".to_string()); // row 25 from column C till H included
		ranges.push("25:H25".to_string()); // row 25 from the first column till column H included

		// multi column/row
		ranges.push("C:F".to_string()); // columns C till F included
		ranges.push("C4:F".to_string()); // columns C till F included starting from row 4
		ranges.push("7:11".to_string()); // row 7 till 11 included
		ranges.push("E14:G64".to_string());

		// multi ranges
		ranges.push("C4:F,E14:G64".to_string());

		let converted_ranges = convert_spread_sheet_ranges(&sheets, &ranges).unwrap();
		assert_eq!(converted_ranges.len(), ranges.len());
		// "C:"
		assert_eq!(converted_ranges[0].len(), 1);
		assert_eq!(converted_ranges[0][0], (0, 0, 2, -1, 2));
		// "H25:H"
		assert_eq!(converted_ranges[1].len(), 1);
		assert_eq!(converted_ranges[1][0], (0, 24, 7, -1, 7));
		// "L25:L37"
		assert_eq!(converted_ranges[2].len(), 1);
		assert_eq!(converted_ranges[2][0], (0, 24, 11, 36, 11));
		// "SecondSheetName.L103:L135"
		assert_eq!(converted_ranges[3].len(), 1);
		assert_eq!(converted_ranges[3][0], (1, 102, 11, 134, 11));
		// "3.L103:L135"
		assert_eq!(converted_ranges[4].len(), 1);
		assert_eq!(converted_ranges[4][0], (2, 102, 11, 134, 11));
		// "T:T49"
		assert_eq!(converted_ranges[5].len(), 1);
		assert_eq!(converted_ranges[5][0], (0, 0, 19, 48, 19));
		// "ABC33:ABC59"
		assert_eq!(converted_ranges[6].len(), 1);
		assert_eq!(converted_ranges[6][0], (0, 32, 626, 58, 626));
		// "25:"
		assert_eq!(converted_ranges[7].len(), 1);
		assert_eq!(converted_ranges[7][0], (0, 24, 0, 24, -1));
		// "C25:25"
		assert_eq!(converted_ranges[8].len(), 1);
		assert_eq!(converted_ranges[8][0], (0, 24, 2, 24, -1));
		// "C25:H25"
		assert_eq!(converted_ranges[9].len(), 1);
		assert_eq!(converted_ranges[9][0], (0, 24, 2, 24, 7));
		// "25:H25"
		assert_eq!(converted_ranges[10].len(), 1);
		assert_eq!(converted_ranges[10][0], (0, 24, 0, 24, 7));

		// multi column/row
		// "C:F"
		assert_eq!(converted_ranges[11].len(), 1);
		assert_eq!(converted_ranges[11][0], (0, 0, 2, -1, 5));
		// "C4:F"
		assert_eq!(converted_ranges[12].len(), 1);
		assert_eq!(converted_ranges[12][0], (0, 3, 2, -1, 5));
		// "7:11"
		assert_eq!(converted_ranges[13].len(), 1);
		assert_eq!(converted_ranges[13][0], (0, 6, 0, 10, -1));
		// "E14:G64"
		assert_eq!(converted_ranges[14].len(), 1);
		assert_eq!(converted_ranges[14][0], (0, 13, 4, 63, 6));

		// multi ranges
		// "C4:F,E14:G64"
		assert_eq!(converted_ranges[15].len(), 2);
		assert_eq!(converted_ranges[15][0], (0, 3, 2, -1, 5));
		assert_eq!(converted_ranges[15][1], (0, 13, 4, 63, 6));
	}
}
