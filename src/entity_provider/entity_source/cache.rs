// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use crate::entity_provider::entity_source::data::EntitySourceData;

#[derive(Debug)]
pub struct Cache {
    source_cache: HashMap<String, Arc<Vec<(String, Vec<Vec<String>>)>>>,
    entity_source_cache: HashMap<String, Arc<EntitySourceData>>,
}

impl Cache {
    pub fn get() -> Arc<Mutex<Cache>> {
        lazy_static! {
            pub static ref CACHE: Arc<Mutex<Cache>> = Arc::new(Mutex::new(Cache {
                source_cache: HashMap::new(),
                entity_source_cache: HashMap::new(),
            }));
        }
        CACHE.to_owned()
    }

    pub fn entity_source_cache_contains(&self, key: &str) -> bool {
        self.entity_source_cache.contains_key(key)
    }

    pub fn entity_source_cache_insert(
        &mut self,
        key: String,
        value: EntitySourceData,
    ) -> Option<Arc<EntitySourceData>> {
        self.entity_source_cache.insert(key, Arc::new(value))
    }

    pub fn entity_source_cache_get(&self, key: &str) -> Option<Arc<EntitySourceData>> {
        Some(self.entity_source_cache.get(key).unwrap().clone())
    }

    pub fn source_cache_contains(&self, key: &str) -> bool {
        self.source_cache.contains_key(key)
    }

    pub fn source_cache_insert(
        &mut self,
        key: String,
        value: Vec<(String, Vec<Vec<String>>)>,
    ) -> Option<Arc<Vec<(String, Vec<Vec<String>>)>>> {
        self.source_cache.insert(key, Arc::new(value))
    }

    pub fn source_cache_get(&self, key: &str) -> Option<Arc<Vec<(String, Vec<Vec<String>>)>>> {
        Some(self.source_cache.get(key).unwrap().clone())
    }
}
