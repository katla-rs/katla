// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

pub mod entity_source;
pub mod random;

use crate::config::entity_provider::EntityProvider;
use crate::config::Identifier;
use crate::model::context::Context;
use crate::model::entity::Entity;
use indexmap::IndexMap;
use std::collections::HashMap;
use std::error::Error;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread::JoinHandle;
use uuid::Uuid;

impl EntityProvider {
    pub fn start(&mut self) -> Result<EntityProviderThread, Box<dyn Error>> {
        let identifier = self.get_identifier();
        let (entity_provider_sender, entity_provider_receiver): (
            Sender<EntityProviderAction>,
            Receiver<EntityProviderAction>,
        ) = channel();
        entity_provider_sender
            .send(EntityProviderAction::Start)
            .unwrap();
        //let entity_provider = self.clone();

        let join_handle = std::thread::Builder::new()
            .name(identifier)
            .spawn(move || loop {
                let entity_provider_action = entity_provider_receiver.recv().unwrap();
                match entity_provider_action.clone() {
                    EntityProviderAction::Start => {}

                    EntityProviderAction::Stop => {
                        break;
                    }
                }
            })?;

        Ok(EntityProviderThread {
            join_handle: Some(join_handle),
            sender: Some(entity_provider_sender),
        })
    }
}

#[derive(Debug)]
pub struct EntityProviderThread {
    join_handle: Option<JoinHandle<()>>,
    sender: Option<Sender<EntityProviderAction>>,
}

// Cloned EntityProviderThread does not have the join_handle
impl Clone for EntityProviderThread {
    fn clone(&self) -> EntityProviderThread {
        EntityProviderThread {
            join_handle: None,
            sender: self.sender.clone(),
        }
    }
}
impl EntityProviderThread {
    pub fn get_entity(&self) -> Result<(), Box<dyn Error>> {
        Ok(())
    }

    pub fn stop(&mut self) -> Result<(), Box<dyn Error>> {
        self.sender
            .as_ref()
            .unwrap()
            .send(EntityProviderAction::Stop)
            .unwrap();
        self.join_handle.take().map(JoinHandle::join);
        Ok(())
    }
}

#[derive(Debug, Clone)]
pub enum EntityProviderAction {
    Start,
    Stop,
}

impl EntityProvider {
    pub fn get_entity(
        &mut self,
        entity_providers: &mut HashMap<String, EntityProvider>,
        context: &mut Context,
        include_paths: &Vec<&str>,
    ) -> Result<Entity, Box<dyn Error>> {
        match self {
            EntityProvider::Sequence { .. } => self.get_entity_(),

            EntityProvider::Timestamp { .. } => self.get_entity_(),

            EntityProvider::Map { .. } => {
                self.get_entity_map(entity_providers, include_paths)
            }

            EntityProvider::Random { .. } => self.get_entity_random(include_paths),

            EntityProvider::Uuid { .. } => self.get_entity_(),

            EntityProvider::Eval { .. } => self.get_entity_eval(context),
        }
    }

    fn get_entity_(&mut self) -> Result<Entity, Box<dyn Error>> {
        match self {
            EntityProvider::Sequence {
                sequence,
                start,
                hold,
                increment,
                minimum,
                maximum,
                cycle,
                init,
                counter,
                ..
            } => {
                let mut sequence = sequence.lock().unwrap();
                let mut init = init.lock().unwrap();
                let mut counter = counter.lock().unwrap();

                if !*init {
                    *init = true;
                    *sequence = start.unwrap_or_else(|| 0);
                } else {
                    if *counter % hold.unwrap_or_else(|| 1) == 0 {
                        *sequence += increment.unwrap_or_else(|| 1);
                    }
                    if *sequence > maximum.unwrap_or_else(|| i64::MAX)
                        || *sequence < minimum.unwrap_or_else(|| i64::MIN)
                    {
                        if cycle.unwrap_or_else(|| true) {
                            *sequence = start.unwrap_or_else(|| 0);
                        } else if *sequence > maximum.unwrap_or_else(|| i64::MAX) {
                            *sequence = maximum.unwrap_or_else(|| i64::MAX);
                        } else if *sequence < minimum.unwrap_or_else(|| i64::MIN) {
                            *sequence = minimum.unwrap_or_else(|| i64::MIN);
                        }
                    }
                }
                *counter += 1;

                Ok(Entity::I64(*sequence))
            }

            EntityProvider::Timestamp { name: _ } => {
                use std::time::{SystemTime, UNIX_EPOCH};
                let now = SystemTime::now();
                let duration_since_epoch =
                    now.duration_since(UNIX_EPOCH).expect("Time went backwards");
                let seconds = duration_since_epoch.as_secs() as i64;
                let nanoseconds = duration_since_epoch.subsec_nanos();
                Ok(Entity::Timestamp {
                    seconds: seconds,
                    nanoseconds: Some(nanoseconds),
                    timezone: None,
                })
            }

            EntityProvider::Uuid { name: _ } => {
                Ok(Entity::String(Uuid::new_v4().hyphenated().to_string()))
                //Ok(Entity::String("uuid".to_string()))
            }

            // TODO throw error
            _ => Ok(Entity::String("error".to_string())),
        }
    }

    fn get_entity_map(
        &mut self,
        entity_providers: &mut HashMap<String, EntityProvider>,
        include_paths: &Vec<&str>,
    ) -> Result<Entity, Box<dyn Error>> {
        match self {
            EntityProvider::Map { name: _, map } => {
                let mut entity_map = IndexMap::new();
                let mut context = Context::local();
                for map_entity_provider_element in map.iter() {
                    if let Some(mut entity_provider) =
                        entity_providers.remove(&map_entity_provider_element.entity)
                    {
                        let entity = entity_provider
                            .get_entity(
                                entity_providers,
                                &mut context,
                                include_paths,
                            )
                            .unwrap();
                        entity_providers.insert(
                            map_entity_provider_element.entity.to_string(),
                            entity_provider,
                        );
                        entity_map.insert(
                            map_entity_provider_element.field.to_string(),
                            entity.clone(),
                        );
                        context.insert(Some(map_entity_provider_element.field.to_string()), entity);
                    }
                }
                Ok(Entity::Map(entity_map))
            }

            // TODO throw error
            _ => Ok(Entity::String("error".to_string())),
        }
    }

    fn get_entity_random(
        &mut self,
        include_paths: &Vec<&str>,
    ) -> Result<Entity, Box<dyn Error>> {
        match self {
            EntityProvider::Random {
                name: _,
                uri: _,
                source,
                array_size,
            } => match array_size {
                Some(array_size) => {
                    let mut array = Vec::new();
                    for _i in 0..*array_size {
                        let entity = random::get_random_entity_from_source(
                            source,
                            &include_paths.clone(),
                        )
                        .unwrap();
                        array.push(entity);
                    }
                    Ok(Entity::Array(array))
                }
                None => random::get_random_entity_from_source(source, include_paths),
            },

            // TODO throw error
            _ => Ok(Entity::String("error".to_string())),
        }
    }

    fn get_entity_eval(&mut self, context: &mut Context) -> Result<Entity, Box<dyn Error>> {
        match self {
            EntityProvider::Eval {
                name: _,
                expression,
            } => {
                crate::eval::expression(context, expression)
                // TODO TODO TODO TODO TODO TODO TODO TODO TODO
                //Ok(Entity::String(Uuid::new_v4().to_hyphenated().to_string()))
            }

            // TODO throw error
            _ => Ok(Entity::String("error".to_string())),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::{SystemTime, UNIX_EPOCH};

    #[test]
    fn test_sequence_get_entity_() {
        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: 1,
	hold: 1,
	increment: 1,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "2");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "3");
            }
            _ => assert!(false),
        }

        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: -1,
	hold: 3,
	increment: 11,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
            }
            _ => assert!(false),
        }

        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: -1,
	hold: 2,
	increment: 11,
	minimum: -9,
	maximum: 23,
	cycle: true,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "21");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "21");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
            }
            _ => assert!(false),
        }

        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: -1,
	hold: 2,
	increment: 11,
	minimum: -9,
	maximum: 23,
	cycle: false,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "10");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "21");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "21");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "23");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "23");
            }
            _ => assert!(false),
        }

        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: -1,
	hold: 2,
	increment: -11,
	minimum: -19,
	maximum: 23,
	cycle: true,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
            }
            _ => assert!(false),
        }

        let mut sequence: EntityProvider = ron::from_str(
            &"(
	type: \"sequence\",
	name: \"name-identifier\",
	start: -1,
	hold: 2,
	increment: -11,
	minimum: -19,
	maximum: 23,
	cycle: false,
)",
        )
        .unwrap();

        match sequence {
            EntityProvider::Sequence { .. } => {
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-1");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-12");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-19");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-19");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-19");
                assert_eq!(sequence.get_entity_().unwrap().to_string(), "-19");
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn test_timestamp_get_entity_() {
        let mut timestamp: EntityProvider = ron::from_str(
            &"(
	type: \"timestamp\",
	name: \"name-identifier\",
)",
        )
        .unwrap();

        if let EntityProvider::Timestamp { .. } = timestamp {
            let earlier = SystemTime::now();
            let timestamp = timestamp.get_entity_().unwrap();
            let later = SystemTime::now();
            if let Entity::Timestamp {
                seconds,
                nanoseconds,
                timezone,
            } = timestamp
            {
                assert_ne!(nanoseconds, None);
                assert_eq!(timezone, None);
                assert!(
                    ((seconds as u128) * 1_000_000_000) + (nanoseconds.unwrap() as u128)
                        >= earlier.duration_since(UNIX_EPOCH).unwrap().as_nanos()
                );
                assert!(
                    ((seconds as u128) * 1_000_000_000) + (nanoseconds.unwrap() as u128)
                        <= later.duration_since(UNIX_EPOCH).unwrap().as_nanos()
                );
            } else {
                assert!(false);
            }
        } else {
            assert!(false);
        }
    }
}
