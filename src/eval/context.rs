// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::eval::EvalError;
use crate::model::context::Context;
use crate::model::entity::Entity;

impl Context {
    pub fn entity_from_path(&self, path: String) -> Result<&Entity, EvalError> {
        debug!("context = {:#?}", self);
        debug!("path = {:#?}", path);
        Err(EvalError::NotImplemented)
    }

    pub fn entity_from_key(&self, key: String) -> Result<&Entity, EvalError> {
        debug!("context = {:#?}", self);
        debug!("key = {:#?}", key);
        match self.get(&key) {
            Some(entity) => {
                debug!("entity = {:#?}", entity);
                Ok(entity)
            }
            None => Err(EvalError::NoEntityFoundInContextWithKey {
                key: key,
                context: self.clone(),
            }),
        }
    }

    pub fn entity_from_index(&self, index: usize) -> Result<&Entity, EvalError> {
        debug!("context = {:#?}", self);
        debug!("index = {:#?}", index);
        match self.get_index(index) {
            Some((_key, entity)) => {
                debug!("entity = {:#?}", entity);
                Ok(entity)
            },
            None => Err(EvalError::NoEntityFoundInContextAtIndex {
                index: index,
                context: self.clone(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_entity_from_path() {
        // TODO
    }

    #[test]
    fn test_entity_from_key() {
        let mut context = Context::local();
        assert_eq!(
            context
                .entity_from_key("index-0".to_string())
                .err()
                .unwrap(),
            EvalError::NoEntityFoundInContextWithKey {
                key: "index-0".to_string(),
                context: context.clone(),
            }
        );

        context.insert(
            Some("index-0".to_string()),
            Entity::String("index-0".to_string()),
        );
        assert_eq!(
            context
                .entity_from_key("index-0".to_string())
                .unwrap()
                .to_string(),
            "index-0"
        );
        assert_eq!(
            context
                .entity_from_key("index-1".to_string())
                .err()
                .unwrap(),
            EvalError::NoEntityFoundInContextWithKey {
                key: "index-1".to_string(),
                context: context.clone(),
            }
        );
    }

    #[test]
    fn test_entity_from_index() {
        let mut context = Context::local();
        assert_eq!(
            context.entity_from_index(0).err().unwrap(),
            EvalError::NoEntityFoundInContextAtIndex {
                index: 0,
                context: context.clone(),
            }
        );

        context.insert(None, Entity::String("index-0".to_string()));
        assert_eq!(context.entity_from_index(0).unwrap().to_string(), "index-0");
        assert_eq!(
            context.entity_from_index(1).err().unwrap(),
            EvalError::NoEntityFoundInContextAtIndex {
                index: 1,
                context: context.clone(),
            }
        );
    }
}
