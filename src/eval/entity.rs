// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::model::entity::Entity;

use super::EvalError;

impl Entity {
    pub fn entity_from_path(&self, path: String) -> Result<Entity, EvalError> {
        debug!("entity = {:#?}", self);
        debug!("path = {:#?}", path);
        match self {
            Entity::Array(array) => {
                let path_parts: Vec<&str> = path.split("[").collect();
                debug!("path parts {:#?}", path_parts);
                let path_base = path_parts[0];
                debug!("path base {:#?}", path_base);
                let path_index;
                if path_parts.len() > 1 {
                    let path_index_string = &path_parts[1][0..path_parts[1].len() - 1];
                    path_index = Some(path_index_string.parse::<usize>().unwrap());
                    debug!("path index {:#?}", path_index);
                } else {
                    path_index = None;
                }

                let entity = array[path_index.unwrap()].clone();

                debug!("entity = {}", entity);
                Ok(entity)
            }
            Entity::Map(map) => {
                debug!("path = {:#?}", path);
                let paths: Vec<&str> = path.split("/").collect();
                if paths.len() > 1 {
                    let mut entity = map.get(paths[0]).unwrap();
                    debug!("entity = {}", entity);
                    for path in &paths[1..] {
                        let path_parts: Vec<&str> = path.split("[").collect();
                        debug!("path parts {:#?}", path_parts);
                        let path_base = path_parts[0];
                        debug!("path base {:#?}", path_base);
                        let path_index;
                        if path_parts.len() > 1 {
                            let path_index_string = &path_parts[1][0..path_parts[1].len() - 1];
                            path_index = Some(path_index_string.parse::<usize>().unwrap());
                            debug!("path index {:#?}", path_index);
                        } else {
                            path_index = None;
                        }
                        match entity {
                            Entity::Map(value) => {
                                if value.contains_key(&path_base.to_string()) {
                                    entity = value.get(&path_base.to_string()).unwrap();
                                } else {
                                    error!("entity '{}' not found", path);
                                }
                            }
                            _ => (),
                        }

                        match entity {
                            Entity::Array(value) => {
                                entity = &value[path_index.unwrap()];
                            }
                            _ => (),
                        }

                        debug!("entity = {}", entity);
                    }
                    Ok(entity.clone())
                } else {
                    Ok(map.get(paths[0]).unwrap().clone())
                }
            }
            _ => Ok(self.clone()),
        }
    }

    pub fn entity_from_key(&self, key: String) -> Result<&Entity, EvalError> {
        debug!("entity = {:#?}", self);
        debug!("key = {:#?}", key);
        match self {
            Entity::Array(array) => {
                let entity;
                let index = key.parse::<usize>();
                match index {
                    Ok(index) => entity = &array[index],
                    Err(_error) => entity = self,
                }
                debug!("entity = {:#?}", entity);
                Ok(entity)
            }
            Entity::Map(map) => match map.get(&key) {
                Some(entity) => {
                    debug!("entity = {:#?}", entity);
                    Ok(entity)
                }
                None => Err(EvalError::NoEntityFoundWithKey {
                    key: key,
                    entity: self.clone(),
                }),
            },
            Entity::Context(context) => context.entity_from_key(key),
            _ => Err(EvalError::NoEntityFoundWithKey {
                key: key,
                entity: self.clone(),
            }),
        }
    }

    pub fn entity_from_index(&self, index: usize) -> Result<&Entity, EvalError> {
        debug!("entity = {:#?}", self);
        debug!("index = {:#?}", index);
        match self {
            Entity::Array(array) => {
                let entity = &array[index];
                debug!("entity = {:#?}", entity);
                Ok(entity)
            }
            Entity::Map(map) => match map.get_index(index) {
                Some((_key, entity)) => {
                    debug!("entity = {:#?}", entity);
                    Ok(entity)
                }
                None => Err(EvalError::NoEntityFoundAtIndex {
                    index: index,
                    entity: self.clone(),
                }),
            },
            Entity::Context(context) => context.entity_from_index(index),
            _ => Err(EvalError::NoEntityFoundAtIndex {
                index: index,
                entity: self.clone(),
            }),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::model::context::Context;

    use super::*;
    use indexmap::IndexMap;

    #[test]
    fn test_entity_from_path() {
        // TODO
    }

    #[test]
    fn test_entity_from_key() {
        assert!(Entity::I8(-8).entity_from_key("8".to_string()).is_err());
        assert!(Entity::U8(8).entity_from_key("8".to_string()).is_err());
        assert!(Entity::I64(-64).entity_from_key("64".to_string()).is_err());
        assert!(Entity::U64(64).entity_from_key("64".to_string()).is_err());
        assert!(Entity::I128(-128)
            .entity_from_key("128".to_string())
            .is_err());
        assert!(Entity::U128(128)
            .entity_from_key("128".to_string())
            .is_err());
        assert!(Entity::String("String".to_string())
            .entity_from_key("3".to_string())
            .is_err());

        let mut array = Vec::new();
        array.push(Entity::I8(-8));
        array.push(Entity::U8(8));
        array.push(Entity::I64(-64));
        array.push(Entity::U64(64));
        array.push(Entity::I128(-128));
        array.push(Entity::U128(128));
        array.push(Entity::String("String".to_string()));
        let entity_array = Entity::Array(array);
        assert_eq!(
            entity_array
                .entity_from_key("4".to_string())
                .unwrap()
                .to_string(),
            "-128"
        );
        assert_eq!(
            entity_array
                .entity_from_key("test".to_string())
                .unwrap()
                .to_string(),
            "{\n\t-8\n\t8\n\t-64\n\t64\n\t-128\n\t128\n\tString\n}"
        );

        let mut map = IndexMap::new();
        map.insert("test I8".to_string(), Entity::I8(-8));
        map.insert("test U8".to_string(), Entity::U8(8));
        map.insert("test I64".to_string(), Entity::I64(-64));
        map.insert("test U64".to_string(), Entity::U64(64));
        map.insert("test I128".to_string(), Entity::I128(-128));
        map.insert("test U128".to_string(), Entity::U128(128));
        map.insert(
            "test String".to_string(),
            Entity::String("String".to_string()),
        );
        let entity_map = Entity::Map(map);
        assert_eq!(
            entity_map
                .entity_from_key("test String".to_string())
                .unwrap()
                .to_string(),
            "String"
        );

        let mut context = Context::local();
        context.insert(Some("test I8".to_string()), Entity::I8(-8));
        context.insert(Some("test U8".to_string()), Entity::U8(8));
        context.insert(Some("test I64".to_string()), Entity::I64(-64));
        context.insert(Some("test U64".to_string()), Entity::U64(64));
        context.insert(Some("test I128".to_string()), Entity::I128(-128));
        context.insert(Some("test U128".to_string()), Entity::U128(128));
        context.insert(
            Some("test String".to_string()),
            Entity::String("String".to_string()),
        );
        let entity_context = Entity::Context(context);
        assert_eq!(
            entity_context
                .entity_from_key("test String".to_string())
                .unwrap()
                .to_string(),
            "String"
        );
        assert!(entity_context
            .entity_from_key("test no String".to_string())
            .is_err());
    }

    #[test]
    fn test_entity_from_index() {
        assert!(Entity::I8(-8).entity_from_index(8).is_err());
        assert!(Entity::U8(8).entity_from_index(8).is_err());
        assert!(Entity::I64(-64).entity_from_index(64).is_err());
        assert!(Entity::U64(64).entity_from_index(64).is_err());
        assert!(Entity::I128(-128).entity_from_index(128).is_err());
        assert!(Entity::U128(128).entity_from_index(128).is_err());
        // TODO have index from String return char at position index
        //assert_eq!(Entity::String("String".to_string()).entity_from_index(3).unwrap().to_string(), "i");
        assert!(Entity::String("String".to_string())
            .entity_from_index(3)
            .is_err());

        let mut array = Vec::new();
        array.push(Entity::I8(-8));
        array.push(Entity::U8(8));
        array.push(Entity::I64(-64));
        array.push(Entity::U64(64));
        array.push(Entity::I128(-128));
        array.push(Entity::U128(128));
        array.push(Entity::String("String".to_string()));
        let entity_array = Entity::Array(array);
        assert_eq!(
            entity_array.entity_from_index(4).unwrap().to_string(),
            "-128"
        );

        let mut map = IndexMap::new();
        map.insert("test".to_string(), Entity::I8(-8));
        map.insert("test".to_string(), Entity::U8(8));
        map.insert("test".to_string(), Entity::I64(-64));
        map.insert("test".to_string(), Entity::U64(64));
        map.insert("test".to_string(), Entity::I128(-128));
        map.insert("test".to_string(), Entity::U128(128));
        map.insert("test".to_string(), Entity::String("String".to_string()));
        let entity_map = Entity::Map(map);
        assert_eq!(
            entity_map.entity_from_index(0).unwrap().to_string(),
            "String"
        );

        let mut context = Context::local();
        context.insert(None, Entity::I8(-8));
        context.insert(None, Entity::U8(8));
        context.insert(None, Entity::I64(-64));
        context.insert(None, Entity::U64(64));
        context.insert(None, Entity::I128(-128));
        context.insert(None, Entity::U128(128));
        context.insert(None, Entity::String("String".to_string()));
        let entity_context = Entity::Context(context);
        assert_eq!(
            entity_context.entity_from_index(4).unwrap().to_string(),
            "-128"
        );
    }
}
