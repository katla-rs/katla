// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::model::context::Context;
use crate::model::entity::Entity;

use rhai::{Engine, EvalAltResult, Scope};
use std::error::Error;
use std::sync::Arc;

fn get_rhai_engine() -> Arc<Engine> {
    lazy_static! {
        pub static ref RHAI: Arc<Engine> = Arc::new(
            {
                let mut rhai = Engine::new();
                // Register the custom type.
                rhai.register_type::<Entity>();
                //rhai.register_get("string_value", Entity::string_value);
                //rhai.register_fn("/", |x: i64, y: i64| (x * y) - (x + y));
                rhai.register_indexer_get(Entity::rhai_indexer_string);
                rhai.register_indexer_get(Entity::rhai_indexer_usize);
                rhai.register_indexer_get(Entity::rhai_indexer_u128);
                rhai.register_indexer_get(Entity::rhai_indexer_u64);
                rhai.register_indexer_get(Entity::rhai_indexer_u32);
                rhai.register_indexer_get(Entity::rhai_indexer_u16);
                rhai.register_indexer_get(Entity::rhai_indexer_u8);
                rhai.register_indexer_get(Entity::rhai_indexer_isize);
                rhai.register_indexer_get(Entity::rhai_indexer_i128);
                rhai.register_indexer_get(Entity::rhai_indexer_i64);
                rhai.register_indexer_get(Entity::rhai_indexer_i32);
                rhai.register_indexer_get(Entity::rhai_indexer_i16);
                rhai.register_indexer_get(Entity::rhai_indexer_i8);

                rhai.register_fn("format", Entity::rhai_format_self);
                rhai.register_fn("format", Entity::rhai_format_self_str);

                rhai
                        }
        );
    }
    RHAI.to_owned()
}

impl Entity {
    pub fn rhai_indexer_string(&mut self, index: String) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_key(index).unwrap().to_owned())
    }

    pub fn rhai_indexer_usize(&mut self, index: usize) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index).unwrap().to_owned())
    }

    pub fn rhai_indexer_u128(&mut self, index: u128) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_u64(&mut self, index: u64) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_u32(&mut self, index: u32) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_u16(&mut self, index: u16) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_u8(&mut self, index: u8) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_isize(&mut self, index: isize) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_i128(&mut self, index: i128) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_i64(&mut self, index: i64) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_i32(&mut self, index: i32) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_i16(&mut self, index: i16) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_indexer_i8(&mut self, index: i8) -> Result<Entity, Box<EvalAltResult>> {
        Ok(self.entity_from_index(index as usize).unwrap().to_owned())
    }

    pub fn rhai_format_self(&mut self) -> Result<Entity, Box<EvalAltResult>> {
        Ok(Entity::String(self.format(None, None).unwrap().to_owned()))
    }

    pub fn rhai_format_self_str(&mut self, format: &str) -> Result<Entity, Box<EvalAltResult>> {
        Ok(Entity::String(
            self.format(Some(format), None).unwrap().to_owned(),
        ))
    }
}

pub fn template_rhai(context: &Context, template: String) -> Result<String, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("template = {:#?}", template);
    Ok(expression_rhai(context, template).unwrap().to_string())
}

pub fn expression_rhai(context: &Context, expression: String) -> Result<Entity, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("expression = {:#?}", expression);
    let rhai = get_rhai_engine();
    let mut scope = Scope::new();
    for (entity_name, entity) in context.iter() {
        scope.set_value(entity_name.as_str(), entity.clone());
    }

    let result = rhai
        .eval_with_scope::<Entity>(&mut scope, &expression)
        .expect(&expression.to_string());

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eval::tests::create_test_context;

    #[test]
    fn test_template_rhai_entity_string() {
        let context = create_test_context();

        assert_eq!(
            template_rhai(&context, "entityString".to_string())
                .unwrap()
                .to_string(),
            "String"
        );
    }

    #[test]
    fn test_template_rhai_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            template_rhai(
                &context,
                "entityTimestamp_epoch.format(\"%FT%T%.9f%::z\")".to_string()
            )
            .unwrap(),
            "1970-01-01T00:00:00.000000000+00:00:00"
        );

        assert_eq!(
            template_rhai(&context, "entityTimestamp_epoch".to_string()).unwrap(),
            "1970-01-01T00:00:00"
        );

        assert_eq!(
            template_rhai(&context, "entityTimestamp_22221222T222222_UTC".to_string()).unwrap(),
            "2222-12-22T22:22:22+00:00:00"
        );

        assert_eq!(
            template_rhai(&context, "entityTimestamp_22221222T000000_UTC".to_string()).unwrap(),
            "2222-12-22T00:00:00+00:00:00"
        );

        assert_eq!(
            template_rhai(&context, "entityTimestamp_22221222T235959_UTC".to_string()).unwrap(),
            "2222-12-22T23:59:59+00:00:00"
        );

        assert_eq!(
            template_rhai(
                &context,
                "entityTimestamp_minus22221222T222222_UTC".to_string()
            )
            .unwrap(),
            "-2222-12-22T22:22:22+00:00:00"
        );
    }

    #[test]
    fn test_expression_rhai_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            expression_rhai(
                &context,
                "entityTimestamp_epoch.format(\"%FT%T%.9f%::z\")".to_string()
            )
            .unwrap(),
            Entity::String("1970-01-01T00:00:00.000000000+00:00:00".to_owned())
        );

        assert_eq!(
            expression_rhai(&context, "entityTimestamp_epoch".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 0,
                nanoseconds: None,
                timezone: None
            }
        );

        assert_eq!(
            expression_rhai(&context, "entityTimestamp_22221222T222222_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983094942,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_rhai(&context, "entityTimestamp_22221222T000000_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983014400,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_rhai(&context, "entityTimestamp_22221222T235959_UTC".to_string()).unwrap(),
            Entity::Timestamp {
                seconds: 7983100799,
                nanoseconds: None,
                timezone: Some(0)
            }
        );

        assert_eq!(
            expression_rhai(
                &context,
                "entityTimestamp_minus22221222T222222_UTC".to_string()
            )
            .unwrap(),
            Entity::Timestamp {
                seconds: -132255941858,
                nanoseconds: None,
                timezone: Some(0)
            }
        );
    }

    #[test]
    fn test_template_rhai_entity_map() {
        let context = create_test_context();
        assert_eq!(
            template_rhai(
                &context,
                "entityMap".to_string()
            )
            .unwrap(),
            "{\n\t\"child_map_entityI8\": -8\n\t\"child_map_entityU8\": 8\n\t\"child_map_entityI64\": -64\n\t\"child_map_entityU64\": 64\n\t\"child_map_entityI128\": -128\n\t\"child_map_entityU128\": 128\n\t\"child_map_entityString\": Map String\n}"
        );
    }

    #[test]
    fn test_template_rhai_entity_map_indexed() {
        let context = create_test_context();
        assert_eq!(
            template_rhai(
                &context,
                "entityMap[\"child_map_entityString\"]".to_string()
            )
            .unwrap(),
            "Map String"
        );
    }

    #[test]
    fn test_template_rhai_entity_array() {
        let context = create_test_context();
        assert_eq!(
            template_rhai(&context, "entityArray".to_string()).unwrap(),
            "{\n\t-8\n\t8\n\t-64\n\t64\n\t-128\n\t128\n\tArray String\n}"
        );
    }

    #[test]
    fn test_template_rhai_entity_array_indexed() {
        let context = create_test_context();
        assert_eq!(
            template_rhai(&context, "entityArray[6]".to_string()).unwrap(),
            "Array String"
        );
    }
}
