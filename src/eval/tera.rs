// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use super::super::model::entity::Entity;

use chrono::DateTime;
use serde_json::value::Value;
use serde_json::Number;
use std::collections::HashMap;
use std::error::Error;
use std::time::{SystemTime, UNIX_EPOCH};
use tera::Context;
use tera::Tera;

pub fn get_tera_engine() -> Tera {
    lazy_static! {
        pub static ref TERA: Tera = {
            let mut tera = Tera::default();
            tera.register_filter("now", tera_now);
            tera.register_filter("format", tera_format);
            tera.register_filter("formatTimestamp", tera_format_timestamp);
            tera
        };
    };
    TERA.to_owned()
}

pub fn template_tera(
    context: &crate::model::context::Context,
    template: String,
) -> Result<String, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("template = {:#?}", template);
    let mut tera = get_tera_engine();
    let result = tera.render_str(
        &template,
        &Context::from_serialize(&context.get_entities()).unwrap(),
    );
    Ok(result.unwrap())
}

pub fn expression_tera(
    context: &crate::model::context::Context,
    expression: String,
) -> Result<Entity, Box<dyn Error>> {
    log::info!("context = {:#?}", context);
    log::info!("expression = {:#?}", expression);
    let result = template_tera(context, vec!["{{ ", &expression, " }}"].concat());
    Ok(Entity::String(result.unwrap()))
}

fn tera_now(_value: &Value, _args: &HashMap<String, Value>) -> tera::Result<Value> {
    let start = SystemTime::now();
    let since_the_epoch = start.duration_since(UNIX_EPOCH).unwrap().as_millis() as f64;
    Ok(Value::Number(Number::from_f64(since_the_epoch).unwrap()))
}

fn tera_format(value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
    if is_timestamp_value(value) {
        return tera_format_timestamp(value, args);
    }
    let format = args.get("format");
    match format {
        Some(format) => match format {
            Value::String(format) => format_value(value, format.to_string()),
            _ => Ok(Value::String("".to_string())),
        },
        None => Ok(Value::String("".to_string())),
    }
}

fn tera_format_timestamp(value: &Value, args: &HashMap<String, Value>) -> tera::Result<Value> {
    let format = args.get("format");
    match format {
        Some(format) => {
            match format {
                Value::String(format) => {
                    match value {
                        Value::Number(value) => {
                            // Convert the timestamp string into an i64
                            //let value = value.parse::<i64>().unwrap();

                            // Create a NaiveDateTime from the timestamp
                            let date_time = DateTime::from_timestamp(
                                value.as_i64().unwrap_or_default() / 1000,
                                ((value.as_u64().unwrap_or_default() % 1000 as u64) * 1000000)
                                    as u32,
                            )
                            .unwrap();

                            Ok(Value::String(
                                super::format_date_time_utc(date_time, format.to_string()).unwrap(),
                            ))
                        }
                        Value::Object(..) => Ok(Value::String(
                            value_to_timestamp(value)
                                .format(Some(format), None)
                                .unwrap(),
                        )),
                        // Nul, Bool(bool), Array(Vec<Value>)
                        _ => Ok(Value::String("__UNDEFINED__".to_string())),
                    }
                }
                _ => Ok(Value::String("__UNDEFINED__".to_string())),
            }
        }
        None => Ok(Value::String("__UNDEFINED__".to_string())),
    }
}

fn format_value(value: &Value, format: String) -> tera::Result<Value> {
    log::info!("value = {:?}, format = {:?}", value, format);
    match value {
        Value::Number(value) => {
            if value.is_f64() {
                Ok(Value::String(
                    super::format_f64(value.as_f64().unwrap_or_default(), format).unwrap(),
                ))
            } else if value.is_i64() {
                Ok(Value::String(
                    super::format_i64(value.as_i64().unwrap_or_default(), format).unwrap(),
                ))
            } else if value.is_u64() {
                Ok(Value::String(
                    super::format_u64(value.as_u64().unwrap_or_default(), format).unwrap(),
                ))
            } else {
                Ok(Value::String(
                    super::format_f64(value.as_f64().unwrap_or_default(), format).unwrap(),
                ))
            }
        }
        Value::Array(value) => format_array(value, format),
        _ => Ok(Value::String("".to_string())),
    }
}

fn format_array(value: &Vec<Value>, format: String) -> tera::Result<Value> {
    Ok(Value::String(
        value
            .iter()
            .map(|v| {
                format_value(v, format.clone())
                    .unwrap()
                    .as_str()
                    .unwrap()
                    .to_string()
            })
            .collect::<String>(),
    ))
}

fn is_timestamp_value(value: &Value) -> bool {
    if let Value::Object(value) = value {
        // check if object has three element: seconds, nanoseconds, timezone
        // if so threat it as a Entity::Timestamp
        if !value.contains_key("seconds")
            || !value.contains_key("nanoseconds")
            || !value.contains_key("timezone")
        {
            return false;
        }

        match value.get("seconds").unwrap() {
            Value::Number(number) => {
                if !number.is_i64() {
                    return false;
                };
            }
            _ => return false,
        }

        match value.get("nanoseconds").unwrap() {
            Value::Number(number) => {
                if !number.is_u64() {
                    return false;
                };
            }
            Value::Null => (),
            _ => return false,
        }

        match value.get("timezone").unwrap() {
            Value::Number(number) => {
                if !number.is_i64() {
                    return false;
                };
            }
            Value::Null => (),
            _ => return false,
        }
        return true;
    }
    // anything else is certainly not a timestamp
    false
}

fn value_to_timestamp(value: &Value) -> Entity {
    if is_timestamp_value(value) {
        if let Value::Object(value) = value {
            // get seconds, nanoseconds, timezone from Object
            let seconds_value = value.get("seconds").unwrap();
            let seconds = match seconds_value {
                Value::Number(number) => number.as_i64().unwrap(),
                _ => 0 as i64,
            };

            let nanoseconds_value = value.get("nanoseconds").unwrap();
            let nanoseconds = match nanoseconds_value {
                Value::Number(number) => Some(number.as_u64().unwrap() as u32),
                Value::Null => None,
                _ => None,
            };

            let timezone_value = value.get("timezone").unwrap();
            let timezone = match timezone_value {
                Value::Number(number) => Some(number.as_i64().unwrap() as i32),
                Value::Null => None,
                _ => None,
            };

            return Entity::Timestamp {
                seconds: seconds,
                nanoseconds: nanoseconds,
                timezone: timezone,
            };
        }
    }
    // TODO eror
    Entity::Timestamp {
        seconds: 0,
        nanoseconds: None,
        timezone: None,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::eval::tests::create_test_context;

    #[test]
    fn test_template_tera_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            template_tera(
                &context,
                "{{ entityTimestamp_epoch | format(format='%FT%T%.9f%::z') }}".to_string()
            )
            .unwrap(),
            "1970-01-01T00:00:00.000000000+00:00:00"
        );

        assert_eq!(
            template_tera(
                &context,
                "{{ entityTimestamp_epoch | formatTimestamp(format='%Y-%m-%dT%H:%M:%S%Z') }}"
                    .to_string()
            )
            .unwrap(),
            "1970-01-01T00:00:00+00:00"
        );

        assert_eq!(
            template_tera(&context, "{{ entityTimestamp_22221222T222222_UTC | formatTimestamp(format='%Y-%m-%dT%H:%M:%S%Z') }}".to_string()).unwrap(),
            "2222-12-22T22:22:22+00:00"
        );

        assert_eq!(
            template_tera(&context, "{{ entityTimestamp_22221222T000000_UTC | formatTimestamp(format='%Y-%m-%dT%H:%M:%S%Z') }}".to_string()).unwrap(),
            "2222-12-22T00:00:00+00:00"
        );

        assert_eq!(
            template_tera(&context, "{{ entityTimestamp_22221222T235959_UTC | formatTimestamp(format='%Y-%m-%dT%H:%M:%S%Z') }}".to_string()).unwrap(),
            "2222-12-22T23:59:59+00:00"
        );

        assert_eq!(
            template_tera(&context, "{{ entityTimestamp_minus22221222T222222_UTC | formatTimestamp(format='%Y-%m-%dT%H:%M:%S%Z') }}".to_string()).unwrap(),
            "-2222-12-22T22:22:22+00:00"
        );
    }

    #[test]
    fn test_expression_tera_entity_timestamp() {
        let context = create_test_context();

        assert_eq!(
            expression_tera(
                &context,
                "entityTimestamp_epoch | json_encode()".to_string()
            )
            .unwrap(),
            Entity::String("{\"nanoseconds\":null,\"seconds\":0,\"timezone\":null}".to_string())
        );

        assert_eq!(
            expression_tera(
                &context,
                "entityTimestamp_22221222T222222_UTC | json_encode()".to_string()
            )
            .unwrap(),
            Entity::String(
                "{\"nanoseconds\":null,\"seconds\":7983094942,\"timezone\":0}".to_string()
            )
        );

        assert_eq!(
            expression_tera(
                &context,
                "entityTimestamp_22221222T000000_UTC | json_encode()".to_string()
            )
            .unwrap(),
            Entity::String(
                "{\"nanoseconds\":null,\"seconds\":7983014400,\"timezone\":0}".to_string()
            )
        );

        assert_eq!(
            expression_tera(
                &context,
                "entityTimestamp_22221222T235959_UTC | json_encode()".to_string()
            )
            .unwrap(),
            Entity::String(
                "{\"nanoseconds\":null,\"seconds\":7983100799,\"timezone\":0}".to_string()
            )
        );

        assert_eq!(
            expression_tera(
                &context,
                "entityTimestamp_minus22221222T222222_UTC | json_encode()".to_string()
            )
            .unwrap(),
            Entity::String(
                "{\"nanoseconds\":null,\"seconds\":-132255941858,\"timezone\":0}".to_string()
            )
        );
    }

    #[test]
    fn test_template_tera() {
        let context = create_test_context();

        assert_eq!(
            template_tera(&context, "{{entityString}}".to_string()).unwrap(),
            "String"
        );
        assert_eq!(
            template_tera(&context, "{{ entityString }}".to_string()).unwrap(),
            "String"
        );

        assert_eq!(
            template_tera(
                &context,
                "{{entityMap['child_map_entityString']}}".to_string()
            )
            .unwrap(),
            "Map String"
        );
        assert_eq!(
            template_tera(
                &context,
                "{{ entityMap['child_map_entityString'] }}".to_string()
            )
            .unwrap(),
            "Map String"
        );

        assert_eq!(
            template_tera(&context, "{{entityArray[6]}}".to_string()).unwrap(),
            "Array String"
        );
        assert_eq!(
            template_tera(&context, "{{ entityArray[6] }}".to_string()).unwrap(),
            "Array String"
        );
    }
}
