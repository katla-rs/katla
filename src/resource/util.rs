// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use crate::config::resource::Resource;

impl Resource {
    pub fn media_type(&self) -> Option<&str> {
        let media_type = match self {
            Resource::Data { media_type, .. } => Some(media_type),
            Resource::File { media_type, .. } => media_type.as_ref(),
        };
        match media_type {
            Some(media_type) => Some(&media_type.as_str()),
            None => None,
        }
    }

    pub fn get_string(&self) -> Result<String, String> {
        match self {
            Resource::Data { data, .. } => Ok(data.to_string()),
            Resource::File { path: file, .. } => {
                let result = std::fs::read_to_string(file.clone());
                match result {
                    Ok(result) => Ok(result),
                    Err(_error) => {
                        // TODO be more verbose about the nature of the error
                        Err(file.to_string())
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn test_resource_get_string() {
        let mut base_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        base_dir.push("resources/tests/config/resource/");

        assert_eq!(
            Resource::Data {
                media_type: "".to_string(),
                data: "eval".to_string()
            }
            .get_string(),
            Ok("eval".to_string())
        );
        assert_eq!(
            Resource::File {
                media_type: None,
                authority: None,
                path: format!(
                    "{}{}",
                    base_dir.to_str().unwrap().to_owned(),
                    "get_resource_test.txt".to_string()
                ),
            }
            .get_string(),
            Ok("get_resource_file_name\n".to_string())
        );
    }
}
