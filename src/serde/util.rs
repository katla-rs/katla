// --
// katla is a data flow generator
// --
// Copyright (C) 2019 Twimpiex <twimpiex@gnx.be>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 3 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
// --
// Be aware that at any time this program may introduce bugs, backward
// incompatibilities, undesired behaviour, backdoors, ...
// Trust your testing and quality assurance, don't trust on semantic
// versioning.
//
// Contact the copyright owner if you wish to use this program with
// another license.
// --

use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fmt::Debug;
use std::marker::PhantomData;
use std::str::FromStr;
use serde::{Deserialize, Deserializer};
use serde::de::{self, DeserializeSeed, Visitor, MapAccess, SeqAccess};

use super::error::SerDeError;

pub fn string_or_map<'de, T, D>(deserializer: D) -> Result<T, D::Error>
where
    T: Deserialize<'de> + FromStr<Err = SerDeError>,
    D: Deserializer<'de>,
{
	deserializer.deserialize_any(StringOrMapVisitor(PhantomData))
}

struct StringOrMapVisitor<T>(PhantomData<T>);
impl<'de, T> Visitor<'de> for StringOrMapVisitor<T>
where
	T: Deserialize<'de> + FromStr<Err = SerDeError>,
{
	type Value = T;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("string or map")
	}

	fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
	where
		E: de::Error,
	{
		Ok(FromStr::from_str(value).unwrap())
	}

	fn visit_map<M>(self, map: M) -> Result<Self::Value, M::Error>
	where
		M: MapAccess<'de>,
	{
		T::deserialize(de::value::MapAccessDeserializer::new(map))
	}
}

pub fn option_string_or_map<'de, T, D>(deserializer: D) -> Result<Option<T>, D::Error>
where
    T: Deserialize<'de> + FromStr<Err = SerDeError>,
    D: Deserializer<'de>,
{
	deserializer.deserialize_option(OptionStringOrMapVisitor(PhantomData))
}

// https://docs.serde.rs/src/serde/de/impls.rs.html#589
struct OptionStringOrMapVisitor<T>(PhantomData<T>);
impl<'de, T> Visitor<'de> for OptionStringOrMapVisitor<T>
where
	T: Deserialize<'de> + FromStr<Err = SerDeError>,
{
	type Value = Option<T>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("option string or map")
	}

	fn visit_none<E>(self) -> Result<Self::Value, E>
	where
		E: Error,
	{
		Ok(None)
	}

	fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
	where
		D: Deserializer<'de>,
	{
		Ok(Some(deserializer.deserialize_any(StringOrMapVisitor(PhantomData))?))
	}
}

struct SeqStringOrMapSeed<'a, T: 'a>(&'a mut Vec<T>);
impl<'de, 'a, T> DeserializeSeed<'de> for SeqStringOrMapSeed<'a, T>
where
    T: Deserialize<'de> + FromStr<Err = SerDeError>,
{
    type Value = ();

    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
    where
        D: Deserializer<'de>,
    {
		self.0.push(deserializer.deserialize_any(StringOrMapVisitor(PhantomData))?);
		Ok(())
    }
}

pub fn seq_string_or_map<'de, T, D>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    T: Deserialize<'de> + FromStr<Err = SerDeError> + Debug,
    D: Deserializer<'de>,
{
	deserializer.deserialize_seq(SeqStringOrMapVisitor(PhantomData))
}

//use serde::de::EnumAccess;
struct SeqStringOrMapVisitor<T>(PhantomData<T>);
impl<'de, T> Visitor<'de> for SeqStringOrMapVisitor<T>
where
	T: Deserialize<'de> + FromStr<Err = SerDeError> + Debug,
{
	type Value = Vec<T>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("seq string or map")
	}

	fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
		let mut vec = Vec::new();
		while let Some(()) = seq.next_element_seed(SeqStringOrMapSeed(&mut vec))? {
			// Nothing to do; inner string_or_map has been appended into `vec`.
		}
		Ok(vec)
	}
}

pub fn array_of_struct_with_identifier_as_map<'de, T, D>(deserializer: D) -> Result<HashMap<String, T>, D::Error>
where
	T: Deserialize<'de> + super::super::config::Identifier<Value = String>,
	D: Deserializer<'de>,
{
	let mut sequence: Vec<T> = std::vec::Vec::deserialize(deserializer)?;
	let mut map = HashMap::new();
	while sequence.len() > 0 {
		let element = sequence.remove(0);
		map.insert(element.get_identifier(), element);
	}
	Ok(map)
}

pub fn option_array_of_struct_with_identifier_as_map_working<'de, T, D>(deserializer: D) -> Result<Option<HashMap<String, T>>, D::Error>
where
	T: Deserialize<'de> + super::super::config::Identifier<Value = String>,
	D: Deserializer<'de>,
{
	let mut sequence: Vec<T> = std::vec::Vec::deserialize(deserializer)?;
	let mut map = HashMap::new();
	while sequence.len() > 0 {
		let element = sequence.remove(0);
		map.insert(element.get_identifier(), element);
	}
	Ok(Some(map))
}

pub fn option_array_of_struct_with_identifier_as_map<'de, T, D>(deserializer: D) -> Result<Option<HashMap<String, T>>, D::Error>
where
	T: Deserialize<'de> + super::super::config::Identifier<Value = String>,
	D: Deserializer<'de>,
{
	struct OptionArrayOfStructWithIdentifierAsMapVisitor<T> {
		marker: PhantomData<T>,
	}
/*
	let mut sequence: Vec<T> = std::vec::Vec::deserialize(deserializer)?;
	let mut map = HashMap::new();
	while sequence.len() > 0 {
		let element = sequence.remove(0);
		map.insert(element.get_identifier(), element);
	}
	Ok(Some(map))
*/

	impl<'de, T> Visitor<'de> for OptionArrayOfStructWithIdentifierAsMapVisitor<T>
	where
		T: Deserialize<'de> + super::super::config::Identifier<Value = String>,
	{
        type Value = Option<Vec<T>>;

		fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
			formatter.write_str("option array")
		}

		fn visit_unit<E>(self) -> Result<Self::Value, E>
		where
			E: Error,
		{
			Ok(None)
		}
	
		fn visit_none<E>(self) -> Result<Self::Value, E>
		where
			E: Error,
		{
			Ok(None)
		}
	
		fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
		where
			D: Deserializer<'de>,
		{
			Ok(deserializer.deserialize_any(OptionArrayOfStructWithIdentifierAsMapVisitor { marker: PhantomData }).unwrap())
		}

		fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
		where
			A: SeqAccess<'de>, 
		{
                 // Visit each element in the inner array and push it onto
				 // the existing vector.
			let mut vec = Vec::new();
			while let Some(elem) = seq.next_element()? {
				vec.push(elem);
			}
			if vec.len() > 0 {
				Ok(Some(vec))
			} else {
				Ok(None)
			}
	   }
	}

	let option_sequence: Option<Vec<T>> = deserializer.deserialize_option(OptionArrayOfStructWithIdentifierAsMapVisitor { marker: PhantomData } ).unwrap();
	match option_sequence {
		Some(mut sequence) => {
			let mut map = HashMap::new();
			while let Some(element) = sequence.pop() {
				map.insert(element.get_identifier(), element);
			}
			Ok(Some(map))
		},
		None => Ok(None),
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[derive(Deserialize, Debug, Clone)]
	#[serde(tag = "type", rename_all = "camelCase")]
	pub struct TestStructOptionalEnum {
		name: String,
		#[serde(default)] //  This is needed for the optional to work
		#[serde(deserialize_with = "option_string_or_map")]
		first: Option<TestEnum>,
		#[serde(default)] //  This is needed for the optional to work
		#[serde(deserialize_with = "option_string_or_map")]
		#[serde(rename = "secondEnum")]
		second_enum: Option<TestEnum>,
		//#[serde(deserialize_with = "string_or_struct")]
		#[serde(rename = "thirdEnum")]
		third_enum: TestEnum,
		#[serde(default)] //  This is needed for the optional to work
		#[serde(deserialize_with = "option_string_or_map")]
		#[serde(rename = "fourthEnum")]
		fourth_enum: Option<TestEnum>,
	}

	#[derive(Deserialize, Debug, Clone)]
	#[serde(tag = "type", rename_all = "camelCase")]
	pub enum TestEnum {
		FirstVariant {
			#[serde(rename = "firstOptionalProperty")]
			first_optional_property: Option<String>,
			#[serde(rename = "firstVariantSecondOptionalProperty")]
			first_variant_second_optional_property: Option<String>,
			#[serde(rename = "firstVariantThirdProperty")]
			first_variant_third_property: String,
		},
		SecondVariant {
			#[serde(rename = "firstOptionalProperty")]
			first_optional_property: Option<String>,
			#[serde(rename = "secondtVariantSecondOptionalProperty")]
			second_variant_second_optional_property: Option<Vec<String>>,
			#[serde(rename = "secondVariantThirdProperty")]
			second_variant_third_property: Vec<String>,
		},
	}
/*	
	impl Display for Template {
		fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
			match self {
				Template::Legacy { template, file_name: _ } => {
					write!(f, "{}", template.clone().unwrap())
				},
				Template::Rhai { template, file_name: _ } => {
					write!(f, "{}", template.clone().unwrap())
				},
			}
		}
	}
*/	
	impl FromStr for TestEnum {
		type Err = SerDeError;
	
		fn from_str(string: &str) -> Result<Self, Self::Err> {
			// TODO parse sting in order to also support other templates than legacy
			Ok(TestEnum::FirstVariant {
				first_optional_property: Some(string.to_string()),
				first_variant_second_optional_property: None,
				first_variant_third_property: string.to_string(),
			})
		}
	}

	#[test]
	fn test_optional_string_or_struct() {
		let json = "{
			\"name\": \"Test\",
			\"first\": {
				\"type\": \"firstVariant\",
				\"firstVariantThirdProperty\": \"first Variant Third Property\"
			},
			\"secondEnum\": \"test second enum\",
			\"thirdEnum\": {
				\"type\": \"firstVariant\",
				\"firstVariantThirdProperty\": \"first Variant Third Property\"
			}
		}
		";
		let serde_json: TestStructOptionalEnum = serde_json::from_str(&json).unwrap();

		assert_eq!(serde_json.name, "Test");
		assert_eq!(serde_json.first.is_some(), true);
		match serde_json.first.clone().unwrap() {
			TestEnum::FirstVariant { first_optional_property, first_variant_second_optional_property: _, first_variant_third_property } => {
				assert_eq!(first_optional_property.is_none(), true);
				assert_eq!(first_variant_third_property, "first Variant Third Property");
				//assert_eq!(first_optional_property.is_some(), true);
			},
			_ => assert_eq!(true, false),
		}

		assert_eq!(serde_json.second_enum.is_none(), false);

		match serde_json.third_enum.clone() {
			TestEnum::FirstVariant { first_optional_property, first_variant_second_optional_property: _, first_variant_third_property } => {
				assert_eq!(first_optional_property.is_none(), true);
				assert_eq!(first_variant_third_property, "first Variant Third Property");
				//assert_eq!(first_optional_property.is_some(), true);
			},
			_ => assert_eq!(true, false),
		}

		assert_eq!(serde_json.fourth_enum.is_none(), true);
	}

}
